#!/usr/bin/env python3
from PIL import Image
from lottie.importers.raster import import_raster
from lottie.utils import script
from lottie import Color
from lottie.parsers.svg.importer import parse_color
import lottie.objects


replace_color = Color(1, 1, 1)


def run_image(image, ns, x, y):
    frames = [
        image.crop((x, y + i * ns.tile_height, x + ns.tile_width, y + (i+1) * ns.tile_height))
        for i in range(3)
    ]

    animation = import_raster(frames, 0, [], "pixel", frame_delay=12)

    for style in animation.find_all((lottie.objects.Fill, lottie.objects.Stroke)):
        if style.color.value == replace_color:
            style.color.value = ns.color
        elif ns.alt_color and style.color.value == ns.alt_color[0]:
            style.color.value = ns.alt_color[1]

    script.run(animation, ns)


parser = script.get_parser(formats=["html"])
parser.add_argument("input")
parser.add_argument("--tile-width", "-w", type=int, default=24)
parser.add_argument("--tile-height", "-h", type=int, default=24)
parser.add_argument("color", type=parse_color)
parser.add_argument("first_tile_x", type=int)
parser.add_argument("first_tile_y", type=int)
parser.add_argument("--alt-color", type=parse_color, nargs=2)
parser.add_argument("--type", default="static", choices=["static", "tiled", "directional", "character"])


ns = parser.parse_args()
image = Image.open(ns.input)
x = ns.first_tile_x * ns.tile_width
y = ns.first_tile_y * ns.tile_height
directions = ["right", "up", "left", "down"]


if ns.type == "tiled":
    base_name = ns.name
    for index in range(1, 16):
        name = base_name + "_"
        if index & 8:
            name += "D"
        if index & 4:
            name += "L"
        if index & 2:
            name += "U"
        if index & 1:
            name += "R"
        ns.name = name
        run_image(image, ns, x + index * ns.tile_width, y)
elif ns.type == "directional":
    base_name = ns.name
    for index, direction in enumerate(directions):
        # Skip image from wiki
        if index == 0:
            continue
        ns.name = base_name + "_" + direction
        run_image(image, ns, x + index * ns.tile_width, y)
elif ns.type == "character":
    base_name = ns.name
    for index, direction in enumerate(directions):
        # Skip image from wiki
        if index == 0:
            continue
        ns.name = base_name + "_" + direction
        dx = (index * 5 + 1) * ns.tile_width
        run_image(image, ns, x + dx, y)
else:
    run_image(image, ns, x, y)
