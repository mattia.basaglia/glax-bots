#!/usr/bin/env python3
import os
import json
import string
import pathlib
import subprocess
from lottie.importers.raster import import_raster
from lottie.exporters.core import export_lottie


here = pathlib.Path(__file__).parent
out_path = here / "smol"

with open(here / "data.json") as data_file:
    data = json.load(data_file)


letters = {}

for letter in string.ascii_uppercase:
    letters[letter] = 0
    path = out_path / letter
    path.mkdir(parents=True, exist_ok=True)


words = data["words"].keys()

bad = {
    ("AUTO"),
    ("BABA", 2),
    ("BACK"),
    ("BELT", 2),
    ("BEST"),
    ("BIRD", 0),
    ("BIRD", 2),
    ("BLOB"),
    ("BLUE"),
    ("BOAT", 0),
    ("BOAT", 1),
    ("BOAT", 2),
    ("BOBA", 2),
    ("BOLT", 0),
    ("BOLT", 1),
    ("BOLT", 2),
    ("CAKE", 2),
    ("CART", 0),
    ("CART", 1),
    ("CART", 2),
    ("CART", 3),
    ("CASH", 0),
    ("CASH", 2),
    ("CYAN", 0),
    ("CYAN", 1),
    ("CYAN", 2),
    ("CYAN", 3),
    ("DOWN"),
    ("DOWN", 0),
    ("DOWN", 1),
    ("DOWN", 2),
    ("DOWN", 3),
    ("DRUM", 0),
    ("FALL"),
    ("FEAR", 0),
    ("FIRE", 0),
    ("FIRE", 1),
    ("FIRE", 2),
    ("FISH", 0),
    ("FISH", 1),
    ("FISH", 2),
    ("FOFO", 0),
    ("FOFO", 2),
    ("FOOT", 0),
    ("FORT", 0),
    ("FORT", 2),
    ("FROG", 0),
    ("GATE", 0),
    ("GREY", 0),
    ("GREY", 1),
    ("GREY", 2),
    ("GREY", 3),
    ("HAND", 2),
    ("HUSK", 0),
    ("HUSK", 2),
    ("HUSK", 3),
    ("JIJI", 0), # Modified J0
    ("JIJI", 2), # Modified J1
    ("KEKE", 0),
    ("KEKE", 2),
    ("LAMP", 0),
    ("LAMP", 2),
    ("LAMP", 3),
    ("LAVA", 0),
    ("LEFT"),
    ("LIFT", 0),
    ("LIFT", 2),
    ("LILY", 0),
    ("LILY", 2),
    ("LOCK", 0),
    ("LOCK", 2),
    ("MAKE", 2),
    ("MOVE"),
    ("NOSE", 0),
    ("NOSE", 2),
    ("OPEN"),
    ("PAWN", 0), # Modified P0
    ("PAWN", 2), # Modified W0
    ("PINK", 0),
    ("PINK", 1),
    ("PINK", 2),
    ("PINK", 3),
    ("PIPE", 0), # Modified P1
    ("PIPE", 2), # Modified P2
    ("PLAY", 0),
    ("PULL"),
    ("PUSH"),
    ("RING", 2),
    ("ROSY", 0),
    ("ROSY", 1),
    ("ROSY", 2),
    ("ROSY", 3),
    ("SAFE"),
    ("SEED", 0),
    ("SHUT", 0),
    ("SHUT", 1),
    ("SHUT", 2),
    ("SHUT", 3),
    ("SIGN", 0),
    ("SIGN", 2),
    ("SINK"),
    ("STAR", 0),
    ("STOP"),
    ("SWAP"),
    ("SWAP"),
    ("TURN", 0),
    ("TURN", 1),
    ("TURN", 2),
    ("TURN", 3),
    ("VASE", 0),
    ("VASE", 2),
    ("VINE", 0),
    ("WEAK"),
    ("WHAT", 0),
    ("WIND", 0),
    ("WORD"),
    ("WORM", 0),
    ("YOU2"),
}

custom = {
    "W": ["W0.gif"],
    "P": ["P0.gif", "P1.gif", "P2.gif"],
    "J": ["J0.gif", "J1.gif"],
    "Z": ["Z0.gif"],
    "Q": ["Q0.gif"],
}

print("Custom")
for letter, files in custom.items():
    for index, file in enumerate(files):
        char_path = out_path / letter
        output_gif = char_path / file
        basename = "%s%s" % (letter, index)
        print(basename)
        output_json = char_path / (basename + ".json")
        animation = import_raster([str(output_gif)], 0, [], "pixel")
        letters[letter] = index + 1
        export_lottie(animation, str(output_json))

for word in words:
    word = word.upper()
    info = data["words"][word]
    if len(word) == 4 and word not in bad:
        print(word)
        for y in range(2):
            for x in range(2):
                char_index = y*2+x
                letter = word[char_index]
                input = here / "words" / os.path.basename(info["text-url"])
                if letter not in letters:
                    index = 0
                else:
                    index = letters[letter]

                char_path = out_path / letter
                if index > 3 or not char_path.exists():
                    continue

                if (word, char_index) in bad:
                    continue

                letters[letter] = index + 1

                basename = "%s%s" % (letter, index)
                print(basename)

                output_gif = char_path / (basename + ".gif")
                output_json = char_path / (basename + ".json")
                subprocess.call([
                    "convert",
                    str(input),
                    "-crop", "12x12+%s+%s" % (x * 12, y * 12),
                    "+repage",
                    "-fill", "white", "-colorize", "100%",
                    str(output_gif)
                ])

                animation = import_raster([str(output_gif)], 0, [], "pixel")
                export_lottie(animation, str(output_json))


for letter, index in letters.items():
    if index > 0:
        data["characters"][letter]["smol"] = [
            "smol/{0}/{0}{1}.json".format(letter, i)
            for i in range(index)
        ]
    else:
        data["characters"][letter].pop("smol", None)


with open(here / "data.json", "w") as f:
    json.dump(data, f, indent=4)
