#!/usr/bin/env python3
import sys
import pathlib
from lottie.importers.raster import import_raster
from lottie.exporters.core import export_lottie


input = pathlib.Path(sys.argv[1])

animation = import_raster([str(input)], 0, [], "pixel")
export_lottie(animation, str(input.with_suffix(".json")), True)
