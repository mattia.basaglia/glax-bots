#!/usr/bin/env python3
import sys
import json
import pathlib
from lottie.importers.raster import import_raster
from lottie.exporters.core import export_lottie
from lottie.utils import script
from lottie.utils.color import Color


here = pathlib.Path(__file__).parent
sys.path.append(str(here.parent.parent))
baba = __import__("baba")

parser = script.get_parser(formats=["html"])
parser.add_argument("character")
ns = parser.parse_args()


name = ns.character
input = here / "words" / (name + ".webp")
output = input.with_suffix(".json")
char_animation = import_raster([str(input)], 0, [], "pixel")
export_lottie(char_animation, str(output), True)
best_color = Color(*baba.BabaWriter.best_color(char_animation)) * 255

with open(here / "data.json") as data_file:
    data = json.load(data_file)

data["words"][name.upper()] = {
    "color": "#%02x%02x%02x" % tuple(map(round, best_color.components[:3])),
    "word_type": "noun",
    "picture": {
        "type": "directional",
        "default": "right",
        "right": {
            "file": "words/" + str(output.name)
        }
    }
}

with open(here / "data.json", "w") as output:
    json.dump(data, output, indent=4)

map = baba.BabaMap()
map.add_sentence("{0} {0}!".format(name))
writer = baba.BabaWriter()
writer.set_map(map)
writer.finalize()
script.run(writer.animation, ns)
