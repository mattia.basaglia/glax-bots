import io
import os
import sys
import json
from lxml import etree

input = sys.stdin
input = io.StringIO(r"""
""")
tree = etree.parse(input, etree.HTMLParser())

with open("data.json") as f:
    data = json.load(f)

# Nouns:
# for row in tree.findall("//tr")[1:]:
#     word = row.xpath("td[1]/text()")[0].strip()
#     text_url = row.xpath("td[2]/a/img/@src")[0].split("/revision")[0]
#     picture_url = row.xpath("td[3]/a/img/@src")[0].split("/revision")[0]
#     data.append({
#         "text-url": text_url,
#         "text-file": os.path.basename(text_url).replace(".gif", ".json")
#         "picture-url": picture_url
#         "picture-file": os.path.basename(picture_url).replace(".gif", ".json")
#     })

# Properties:
# for row in tree.findall("//tr"):
#     if row[0].tag == "th":
#         continue
#     word = row.xpath("td[1]/a[1]/@title")[0].strip()
#     text_url = row.xpath("td[1]/a[1]/img/@data-src")[0].split("/revision")[0]
#     data.append({
#         "text-url": text_url,
#         "text-file": os.path.basename(text_url).replace(".gif", ".json")
#     })

# Operators:
# skip = 0
# for row in tree.findall("//tr"):
#     if skip:
#         skip -= 1
#         continue
#
#     if row[0].tag == "th":
#         continue
#
#     skip = int(row.xpath("td[1]/@rowspan")[0]) - 1
#     word = row.xpath("td[1]/a[1]/@title")[0].strip()
#     text_url = row.xpath("td[1]/a[1]/img/@src")[0].split("/revision")[0]
#     data[word] = {
#         "text-url": text_url,
#         "text-file": os.path.basename(text_url).replace(".gif", ".json")
#     }


# Colors https://babaiswiki.fandom.com/wiki/User:TheEmbracedOne/ColorList
def get_pic(td):
    src = td.xpath(".//img/@data-src")
    if not src:
        return None, None
    url = src[0].split("/revision")[0]
    filename = os.path.basename(url).replace(".gif", ".json")
    return url, filename


for link in tree.findall("//td/a[@title]/span"):
    word = link.text
    link_cell = link.getparent().getparent()
    color_cell = link_cell.getnext()
    color = color_cell.text.strip()

    if word not in data["words"]:
        print(word)
        word_data = {
            "color": color
        }
        picture_url, picture_file = get_pic(link_cell.getprevious())
        if not picture_file:
            continue

        text_url, text_file = get_pic(link_cell.getprevious().getprevious())
        if text_file:
            word_data["text-url"] = text_url
            word_data["text-file"] = text_file
            word_data["picture-url"] = picture_url
            word_data["picture-file"] = picture_file
        else:
            word_data["text-url"] = picture_url
            word_data["text-file"] = picture_file

        data["words"][word] = word_data
        print(word_data)
    else:
        data["words"][word]["color"] = color

with open("data.json", "w") as output:
    json.dump(data, output, indent=4)
