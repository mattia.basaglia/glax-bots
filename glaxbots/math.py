import io
import os
import re
import ast
import json
import inspect

import numpy
from PIL import Image, ImageDraw, ImageFilter, ImageMath, ImageOps
from matplotlib import pyplot, font_manager, transforms

import lottie
from lottie.exporters.cairo import export_png
from lottie.utils.font import FontStyle, TextJustify

from mini_apps.telegram.bot import TelegramBot
from mini_apps.telegram.utils import static_sticker_file, send_sticker, InlineHandler
from mini_apps.telegram.events import NewMessageEvent, InlineQueryEvent
from mini_apps.telegram import tl

from .animations import emoji_finder


assets_path = os.path.join(os.path.dirname(__file__), "assets")


class MathParseError(ValueError):
    pass


numpy_funcs = [
    "pi", "e",
    "sin", "cos", "tan", "arcsin", "arccos", "arctan", "arctan2",
    "sinh", "cosh", "tanh", "arcsinh", "arccosh", "arctanh",
    "hypot",
    "floor", "ceil", "trunc",
    "exp", "log", "log2", "log10",
    "fmod", "mod", "modf",
    "angle", "real", "imag", "conj", "conjugate",
    "sqrt", "cbrt",
    "absolute", "abs",
]

numpy_funcs_alias = {
    "arcsin": "asin",
    "arccos": "acos",
    "arctan": "atan",
    "arctan2": "atan2",
    "ln": "log",
}


def plot_visit(node, variables):
    if isinstance(node, ast.Expression):
        return plot_visit(node.body, variables)

    elif isinstance(node, ast.Name):
        if node.id in variables:
            return variables[node.id]
        elif node.id in numpy_funcs:
            return getattr(numpy, node.id)
        elif node.id in numpy_funcs_alias:
            return getattr(numpy, numpy_funcs_alias[node.id])
        else:
            raise MathParseError("Unknown identifier: %s" % node.id)

    elif isinstance(node, ast.Constant):
        if isinstance(node.value, (int, float)):
            return node.value
        else:
            raise MathParseError("Invalid constant: %r" % node.value)

    elif isinstance(node, ast.BinOp):
        a = plot_visit(node.left, variables)
        b = plot_visit(node.right, variables)

        if isinstance(node.op, ast.Add):
            return a + b
        elif isinstance(node.op, ast.Sub):
            return a - b
        elif isinstance(node.op, ast.Mult):
            return a * b
        elif isinstance(node.op, ast.Div):
            return a / b
        elif isinstance(node.op, ast.Mod):
            return a % b
        elif isinstance(node.op, (ast.Pow, ast.BitXor)):
            return a ** b
        else:
            raise MathParseError("Invalid binary operator")

    elif isinstance(node, ast.UnaryOp):
        a = plot_visit(node.operand, variables)

        if isinstance(node.op, ast.UAdd):
            return +a
        elif isinstance(node.op, ast.USub):
            return -a
        else:
            raise MathParseError("Invalid unary operator")

    elif isinstance(node, ast.Call):
        func = plot_visit(node.func, variables)
        args = [plot_visit(a, variables) for a in node.args]
        return func(*args)

    raise MathParseError("Invalid expression")


class BlackBoardItem:
    def build(self):
        """
        Compiles the internal structures
        Post: self.width / self.height set to the size of the image without scaling
        """
        pass

    def render_image(self, scale_x, scale_y):
        """
        Renders the item to an image
        """
        raise NotImplementedError


class PyPlotItem(BlackBoardItem):
    def __init__(
        self,
        fg_color="white",
        bg_color="#00000000",
        dpi=96
    ):
        self.fig = pyplot.figure(dpi=dpi)
        self.fig.set_facecolor(bg_color)
        self.fg_color = fg_color

    def render_plot(self, fig):
        raise NotImplementedError

    def build(self):
        item, bbox = self.render_plot(self.fig)

        self.bbox = bbox
        #x = -bbox.x0 / bbox.height
        #y = -bbox.y0 / bbox.height
        #item.set_position((x, y, 1, 1))

        item.set_position((-bbox.x0 / bbox.width, -bbox.y0 / bbox.height, 1, 1))
        self.fig.set_size_inches((bbox.width / self.fig.dpi, bbox.height / self.fig.dpi))

        self.width = bbox.width
        self.height = bbox.height

    def to_image(self, scale_x, scale_y):
        buffer = io.BytesIO()
        self.fig.savefig(buffer, dpi=self.fig.dpi, format="png")
        buffer.seek(0)

        image = Image.open(buffer)
        image = filter_chalk_text(image)
        return image.resize((int(scale_x * self.width), int(scale_y * self.height)))


class PyPlotText(PyPlotItem):
    def __init__(
        self,
        expr,
        fg_color="white",
        bg_color="#00000000",
        dpi=96,
        style=font_manager.FontProperties(size=50, math_fontfamily="stix"),
    ):
        super().__init__(fg_color, bg_color, dpi)
        self.math_line = expr or " "
        self.style = style

    def render_plot(self, fig):
        try:
            item = fig.text(
                0, 0,
                self.math_line,
                size=50,
                linespacing=1,
                fontproperties=self.style,
                color=self.fg_color,
                horizontalalignment="center",
            )
            bbox = item.get_window_extent(renderer=fig.canvas.get_renderer())
            return item, bbox
        except ValueError as e:
            raise MathParseError(str(e))


class PyPlotPlot(PyPlotItem):
    def __init__(
        self,
        x0, x1, num,
        code,
        scale_x=1, scale_y=1,
        linewidth=6,
        fg_color="white",
        bg_color="#00000000",
        dpi=96
    ):
        super().__init__(fg_color, bg_color, dpi)
        self.input = numpy.linspace(x0, x1, num)
        self.code = code
        self.scale_x = scale_x
        self.scale_y = scale_y
        self.linewidth = linewidth

    @staticmethod
    def range_value(v):
        try:
            return float(v)
        except ValueError:
            return plot_visit(ast.parse(v, mode="eval"), {})

    def plot(self):
        try:
            expr = ast.parse(self.code, mode="eval")
        except SyntaxError as e:
            raise MathParseError(str(e))

        x = self.input
        try:
            y = plot_visit(expr, {"x": x})
        except MathParseError:
            raise
        except Exception as e:
            raise MathParseError(str(e))

        pyplot.plot(x, y, color=self.fg_color, linewidth=self.linewidth)

    def subplot_kwargs(self):
        return {}

    def render_plot(self, fig):
        item = fig.add_subplot(1, 1, 1, **self.subplot_kwargs())
        pyplot.setp(item.spines.values(), visible=False)
        item.set_xticks([])
        item.set_yticks([])
        item.set_facecolor("none")
        item.set_position([0, 0, 1, 1])

        self.plot()
        bbox = item.get_window_extent(renderer=fig.canvas.get_renderer())
        bbox = transforms.Bbox.from_extents(0, 0, bbox.width * self.scale_x, bbox.height * self.scale_y)
        return item, bbox


class PyPlotParametric(PyPlotPlot):
    def plot(self):
        fx, fy = self.code

        try:
            expr_x = ast.parse(fx, mode="eval")
            expr_y = ast.parse(fy, mode="eval")
        except SyntaxError as e:
            raise MathParseError(str(e))

        try:
            t = self.input
            x = plot_visit(expr_x, {"t": t})
            y = plot_visit(expr_y, {"t": t})
        except MathParseError:
            raise
        except Exception as e:
            raise MathParseError(str(e))

        pyplot.plot(x, y, color=self.fg_color, linewidth=self.linewidth)


class PyPlotPolar(PyPlotPlot):
    def subplot_kwargs(self):
        return {"projection": "polar"}

    def plot(self):
        try:
            expr = ast.parse(self.code, mode="eval")
        except SyntaxError as e:
            raise MathParseError(str(e))

        x = self.input
        try:
            y = plot_visit(expr, {"x": x, "theta": x, "angle": x})
        except MathParseError:
            raise
        except Exception as e:
            raise MathParseError(str(e))

        pyplot.polar(x, y, color=self.fg_color, linewidth=self.linewidth)


class PyPlotImplicit(PyPlotPlot):
    def plot(self):
        try:
            expr_l = ast.parse(self.code[0], mode="eval")
            expr_r = ast.parse(self.code[1], mode="eval")
        except SyntaxError as e:
            raise MathParseError(str(e))

        try:
            x, y = numpy.meshgrid(self.input, self.input)
            ctx = {"x": x, "y": y}
            lhs = plot_visit(expr_l, ctx)
            rhs = plot_visit(expr_r, ctx)
        except MathParseError:
            raise
        except Exception as e:
            raise MathParseError(str(e))

        pyplot.contour(lhs - rhs, [0], colors=[self.fg_color], linewidths=[self.linewidth])


class PyPlotPie(PyPlotItem):
    def __init__(
        self,
        values,
        labels=None,
        fg_color="white",
        bg_color="#00000000",
        dpi=96
    ):
        super().__init__(fg_color, bg_color, dpi)
        self.values = values
        self.labels = labels

    @classmethod
    def from_input(cls, input):
        if "[" not in input and "{" not in input and "}" not in input and "}" not in input and "," in input:
            labels = None
            try:
                values = list(map(float, input.split(",")))
            except ValueError:
                raise MathParseError("Invalid chart data")
        else:
            data = json.loads(input)
            if isinstance(data, list):
                values = data
                labels = None
            elif isinstance(data, dict):
                labels = list(data.keys())
                values = list(data.values())
            else:
                raise MathParseError("Invalid chart data")

        return cls(values, labels)

    def render_plot(self, fig):
        try:
            ax = fig.add_axes((0, 0, 1, 1))
            pyplot.pie(
                self.values,
                labels=self.labels,
                colors=["none"],
                textprops={
                    "size": 50,
                    "math_fontfamily": "stix",
                    "family": "cursive"
                },
                wedgeprops={
                    "edgecolor": self.fg_color,
                    "linewidth": 6,
                }
            )
            bbox = fig.get_window_extent(renderer=fig.canvas.get_renderer())
            return ax, bbox
        except ValueError as e:
            raise MathParseError(str(e))


class ImageItem(BlackBoardItem):
    def __init__(self, image):
        self.image = image
        self.width = image.width
        self.height = image.height

    def build(self):
        pass

    def to_image(self, scale_x, scale_y):
        image = filter_chalk_image(self.image)
        return image.resize((int(scale_x * self.width), int(scale_y * self.height)))


class LottieItem(ImageItem):
    def __init__(self, animation):
        super().__init__(animation)
        self.animation = animation

    def build(self):
        data_png = io.BytesIO()
        export_png(self.animation, data_png)
        data_png.seek(0)
        self.image = Image.open(data_png)


class FreeTextItem(BlackBoardItem):
    def __init__(self, text, font=FontStyle("Ubuntu:style=italic", 50, TextJustify.Left, emoji_finder=emoji_finder)):
        self.text = text
        self.font = font

    def build(self):
        self.animation = lottie.objects.Animation()
        self.layer = lottie.objects.ShapeLayer()
        self.animation.add_layer(self.layer)
        self.group = self.font.render(self.text, lottie.NVector(0, 0))
        bbox = self.group.bounding_box()
        self.group.transform.position.value.x = -bbox.x1
        self.group.transform.position.value.y = -bbox.y1
        self.layer.add_shape(self.group)
        self.layer.add_shape(lottie.objects.Fill(lottie.NVector(1, 1, 1)))
        self.width = max(1, self.group.next_x)
        self.height = max(self.font.line_height, bbox.height)

    def to_image(self, scale_x, scale_y):
        self.animation.width = self.width * scale_x
        self.animation.height = self.height * scale_y
        self.layer.transform.scale.value = lottie.NVector(scale_x * 100, scale_y * 100)

        data_png = io.BytesIO()
        export_png(self.animation, data_png)
        data_png.seek(0)
        image = Image.open(data_png)
        return filter_chalk_text(image, False)


class BlackBoardContents:
    re_plot = re.compile(
        r'\\plot\(\s*(?P<x0>[^),]+),\s*(?P<x1>[^),]+)(?:,\s*(?P<step>[^),]+)(?:,\s*(?P<sx>[^),]+)(?:,\s*(?P<sy>[^),]+))?)?)?\)\s*' +
        r'(?P<f>.*)')

    def __init__(self):
        self.items = []
        self.line_gap = 0

    def parse(self, text):
        lines = re.sub(r"\\n\b", "\n", text).split("\n")
        for line in lines:
            if line.startswith("\\plot"):
                match = self.re_plot.match(line)
                if not match:
                    raise MathParseError("Missing plot range")

                x0 = PyPlotPlot.range_value(match.group("x0"))
                x1 = PyPlotPlot.range_value(match.group("x1"))
                try:
                    sx = float(match.group("sx")) if match.group("sx") is not None else 1
                    sy = float(match.group("sy")) if match.group("sy") is not None else sx
                    step = int(match.group("step")) if match.group("step") is not None else 20
                except ValueError:
                    raise MathParseError("Invalid plot parameter")

                f = match.group("f").strip()
                if not f:
                    raise MathParseError("Missing plot function")

                if ";" in f:
                    self.items.append(PyPlotParametric(x0, x1, step, [fi.strip() for fi in f.split(";")], sx, sy))
                elif "=" in f:
                    self.items.append(PyPlotImplicit(x0, x1, step, [fi.strip() for fi in f.split("=")], sx, sy))
                elif "theta" in f:
                    self.items.append(PyPlotPolar(x0, x1, step, f, sx, sy))
                else:
                    self.items.append(PyPlotPlot(x0, x1, step, f, sx, sy))
            elif line.startswith("\\pie"):
                split = line.find(" ")
                if split == -1:
                    raise MathParseError("Missing chart data")
                data = line[split+1:]
                self.items.append(PyPlotPie.from_input(data))
            elif line == "":
                self.items.append(PyPlotText(" "))
            elif len(line) > 1 and line[0] == "$" and line[-1] == "$":
                self.items.append(PyPlotText(line))
            else:
                self.items.append(FreeTextItem(line))

    def build(self):
        for item in self.items:
            item.build()

        self.height = 0
        self.width = 0

        for item in self.items:
            self.height += item.height + self.line_gap
            self.width = max(self.width, item.width)

        self.height -= self.line_gap
        if self.height <= 0:
            self.height = 1

        self.ratio = self.width / self.height

        self.scale_x = 1
        self.scale_y = 1

    def resize(self, width=None, height=None):
        if not height:
            height = width / self.ratio
        elif not width:
            width = height * self.ratio

        self.scale_x = width / self.width
        self.scale_y = height / self.height
        self.width *= self.scale_x
        self.height *= self.scale_y

    def render(self, out_image, center_x, center_y):
        y = int(center_y - self.height / 2)

        for item in self.items:
            image = item.to_image(self.scale_x, self.scale_y)
            out_image.alpha_composite(
                image,
                dest=(int(center_x - image.width / 2), y)
            )
            y += image.height


class BlackBoardRenderer:
    blackboard_l = 233
    blackboard_r = 486
    blackboard_t = 94
    blackboard_b = 314
    blackboard_bounds = [blackboard_l, blackboard_t, blackboard_r, blackboard_b]

    usable_l = blackboard_l + 5
    usable_t = blackboard_t + 2
    usable_r = blackboard_r - 3
    usable_b = 295
    usable_w = usable_r - usable_l
    usable_h = usable_b - usable_t
    usable_ratio = usable_w / usable_h
    usable_l_hand = 305
    usable_b_hand = 234
    usable_w_hand = usable_r - usable_l_hand
    usable_h_hand = usable_b_hand - usable_t

    overlay = Image.open(os.path.join(assets_path, "math.png"))

    def render(self, parsed_item):
        out = Image.new("RGBA", (512, 512), (0, 0, 0, 0))
        draw = ImageDraw.Draw(out)
        draw.rectangle(self.blackboard_bounds, "#292f2b")

        # Small image, don't stretch, center
        if parsed_item.width <= self.usable_w and parsed_item.height < self.usable_h:
            pass
        # Wide image, stretch to width, center y
        elif parsed_item.ratio > self.usable_ratio:
            parsed_item.resize(width=self.usable_w)
        else:
            parsed_item.resize(height=self.usable_h)

        center_y = self.usable_t + self.usable_h / 2

        if parsed_item.width <= self.usable_w_hand and parsed_item.height >= self.usable_h_hand:
            center_x = self.usable_l_hand + self.usable_w_hand / 2
        else:
            center_x = self.usable_l + self.usable_w / 2
            if parsed_item.width > self.usable_w_hand and parsed_item.height < self.usable_h_hand:
                center_y = self.usable_t + self.usable_h_hand / 2

        parsed_item.render(out, center_x, center_y)

        out.alpha_composite(self.overlay)

        return out


class BlackBoard:
    def __init__(self):
        self.renderer = BlackBoardRenderer()
        self.contents = BlackBoardContents()

    def parse(self, text):
        self.contents.parse(text)
        return self

    def render(self):
        self.contents.build()
        return self.renderer.render(self.contents)

    def image(self, image):
        self.contents.items.append(ImageItem(image))
        return self

    def lottie(self, lottie):
        self.contents.items.append(LottieItem(lottie))
        return self


def filter_chalk_text(source, use_alpha=True):
    if use_alpha:
        grayscale = source.getchannel("A")
    else:
        grayscale = source.convert("L")
        alpha = source.getchannel("A")
        grayscale = ImageMath.eval("int(float(a) * 0.6 + 255 * 0.4) * b / 255", a=grayscale, b=alpha).convert("L")

    alpha = grayscale
    edges = alpha

    edges = edges.filter(ImageFilter.GaussianBlur(2))
    edges = edges.filter(ImageFilter.FIND_EDGES)
    edges = edges.filter(ImageFilter.EDGE_ENHANCE_MORE)
    edges = edges.filter(ImageFilter.EDGE_ENHANCE)
    edges = edges.filter(ImageFilter.MedianFilter(3))
    edges = edges.filter(ImageFilter.SMOOTH)
    edges = edges.filter(ImageFilter.SHARPEN)

    edges = ImageMath.eval("(alpha * 6 + edges * 4) / 10", alpha=alpha, edges=edges).convert("L")

    pic = edges.convert("RGBA")
    pic.putalpha(edges)

    return pic


def filter_chalk_image(source):
    grayscale = source.convert("L")
    try:
        alpha = source.getchannel("A")
        grayscale = ImageMath.eval("convert(a * b / 255, 'L')", a=grayscale, b=alpha)
    except ValueError:
        pass

    # Find edges
    edges = grayscale.filter(ImageFilter.SMOOTH).filter(ImageFilter.FIND_EDGES)
    # Remove "frame"
    edges = ImageOps.crop(edges, 1)
    # Make edges thicker and brighter
    edges = edges.filter(ImageFilter.EDGE_ENHANCE_MORE).filter(ImageFilter.MaxFilter(5))

    image = edges.convert("RGBA")
    image.putalpha(edges)

    return image


def math_to_sticker(expr):
    return BlackBoard().parse(expr).render()


max_photo_size = 1024


def document_is_image(document):
    if document.mime_type not in ["image/png", "image/webp", "image/jpeg", "application/x-tgsticker"]:
        return False

    for attribute in document.attributes:
        if isinstance(attribute, tl.types.DocumentAttributeImageSize):
            if attribute.w > max_photo_size or attribute.h > max_photo_size:
                return False
            else:
                return True

    return False


async def render_telegram_document(event, document):
    if not document_is_image(document):
        await render_telegram_media(event, document, document.thumbs)
        return

    buf = io.BytesIO()
    await event.client.download_file(document, buf)
    buf.seek(0)
    blackboard = BlackBoard()

    if document.mime_type == "application/x-tgsticker":
        blackboard.lottie(lottie.importers.core.import_tgs(buf))
    else:
        blackboard.image(Image.open(buf))

    image = blackboard.render()

    await send_sticker(event.client, event.chat, static_sticker_file(image))


async def render_telegram_media(event, media, thumbs):
    best = None
    best_d = 10000
    for size in thumbs:
        d = abs(size.w - BlackBoardRenderer.usable_w)
        if d < best_d and size.w < max_photo_size and size.h < max_photo_size:
            best_d = d
            best = size

    if not best:
        return

    buf = io.BytesIO()
    await event.client.download_media(media, buf, thumb=best)
    buf.seek(0)
    image = BlackBoard().image(Image.open(buf)).render()

    await send_sticker(event.client, event.chat, static_sticker_file(image))


class MathBot(TelegramBot):
    @TelegramBot.bot_command
    async def start(self, args, event: NewMessageEvent):
        """
        Shows help
        """
        text = inspect.cleandoc(r"""
            This bot renders a sticker of Glax writing some Math on a blackboard.

            **Images**

            You can send me an image or sticker and I will render it on the blackboard.
            (This doesn't work in inline mode)

            **Text Mode**

            This works in inline mode and with the /math command.
            You can split lines with newlines or `\n`.
            Each line can be one of the items explained below.

            **Text**

            You can send normal text and include emoji.

            **LaTeX**

            It supports a subset of LaTeX math mode.
            You can check these docs for [syntax](https://docs.latexbase.com/) and [symbols](https://oeis.org/wiki/List_of_LaTeX_mathematical_symbols).

            Latex Math expressions must be surrounded by `$`

            Example:
            ```tex
            $i \hbar \frac{d}{d t}\vert\Psi(t)\rangle = \hat H\vert\Psi(t)\rangle$
            ```
            **Plots**

            As an extension it can plot functions with this syntax:
            ```tex
            \plot(a, b, resolution, scale_x, scale_y) function...
            ```
            All the arguments of `\plot` must be numbers and they are interpreted as follows:

            `a` Starting input value
            `b` Final input value
            `resolution` Number of segments in the input range
            `scale_x`, `scale_y` Make the graph smaller or larger relative to other stuff.

            Only `a` and `b` are required.

            The function code must use Python syntax, it supports several math functions
            and the most common mathematical operators.

            You can plot real functions by specifying an expression referencing `x`:
            ```tex
            \plot(-3, 3) -x**2
            ```
            You can also plot parametric plots by specifying two expressions in `t`
            separated by `;`:
            ```tex
            \plot(0, 2 * pi, 50) cos(t); sin(t*3)
            ```
            You can plot implicit functions by having two expressions separated by `=`
            referencing `x` and `y`:
            ```tex
            \plot(-1.3, 1.3) x**2 = 1 - (5/4 * y - sqrt(abs(x)))**2
            ```
            Polar plots are created using expressions referencing `theta`:
            ```tex
            \plot(0, 2 * pi) theta
            ```
            **Charts**

            You can make a pie chart with the following syntax:
            ```tex
            \pie data
            ```
            Where `data` can be one of the following:

            A comma separated sequence of values:
            ```csv
            1, 2, 3
            ```
            A JSON array of numbers:
            ```json
            [1, 2, 3]
            ```
            A JSON object:
            ```json
            {"Roar": 1, "Meow": 2, "Rawr": 3}
            ```
        """)

        await event.client.send_message(event.chat, text, parse_mode='md')

    @TelegramBot.bot_command
    async def math(self, args, event: NewMessageEvent):
        """
        Shows Glax doing some math
        """
        if not args:
            await event.client.send_message(event.chat, "You have to send me a math expression with that command")
            return

        try:
            await send_sticker(event.client, event.chat, static_sticker_file(BlackBoard().parse(args).render()))
        except MathParseError as e:
            await event.client.send_message(event.chat, str(e))

    async def on_telegram_inline(self, event: InlineQueryEvent):
        async with InlineHandler(event) as inline:
            if inline.query:
                try:
                    inline.sticker(static_sticker_file(BlackBoard().parse(inline.query).render()))
                except MathParseError:
                    pass

    async def on_telegram_message(self, event: NewMessageEvent):
        if event.message.media and not event.sender.is_self:
            if event.message.sticker:
                await render_telegram_document(event, event.message.sticker)
            elif event.message.photo:
                await render_telegram_media(event, event.message.photo, event.message.photo.sizes)
            elif event.message.document:
                await render_telegram_document(event, event.message.document)
            return True
        return False
