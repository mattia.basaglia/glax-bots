from mini_apps.telegram.bot import TelegramBot
from mini_apps.telegram.utils import parse_text


class TestBot(TelegramBot):
    async def on_telegram_message(self, event):
        parsed = await parse_text(event)
        for chunk in parsed:
            self.log.warning(str(chunk))
        self.log.warning("")
