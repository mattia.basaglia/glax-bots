import typing
import pathlib
import warnings
import collections
import dataclasses

import transformers


Convention = collections.namedtuple("Convention", ["convention", "short_name", "location", "country", "start_date", "end_date", "furries_attendees"])


@dataclasses.dataclass
class Answer:
    answer: str
    conventions: typing.List[Convention]


class ConventionAnswerer:
    def __init__(self, modeldir: pathlib.Path):
        self._pipeline = None
        self.modeldir = modeldir
        self.conventions = []
        self.table = {
            name: []
            for name in Convention._fields
        }

    def ensure_pipeline(self, save=True, force_download=False):
        if force_download or not self.modeldir.exists():
            self._pipeline = transformers.pipeline("table-question-answering", model="google/tapas-base-finetuned-wtq")
            if save:
                self._pipeline.save_pretrained(str(self.modeldir))
        else:
            self._pipeline = transformers.pipeline("table-question-answering", str(self.modeldir))

    def add_con(self, convention: Convention):
        self.conventions.append(convention)
        for name, value in convention._asdict().items():
            self.table[name].append(value)

    def answer(self, question):
        if not self._pipeline:
            self.ensure_pipeline()

        raw = self._pipeline(query=question, table=self.table)
        answer = raw["answer"]
        if raw["aggregator"] != "NONE":
            answer = answer[len(raw["aggregator"])+3:]
        return Answer(answer, [self.conventions[c[0]] for c in raw["coordinates"]])
