import io
import json
import socket
import asyncio

import aiohttp
from PIL import Image
from yarl import URL

from mini_apps.telegram.auto_bot import named_bot_command as bot_command, named_bot_inline as bot_inline
from mini_apps.telegram.utils import photo_file, send_sticker, InlineHandler
from mini_apps.telegram.events import NewMessageEvent, InlineQueryEvent

api_base = URL("https://dragon.best/api/")


async def image_common(event, query, path, get_arg):
    if not query:
        await event.client.send_message(event.chat, "You have to send me some text with that command")
        return

    await send_sticker(event.client, event.chat, await get_image(event, query, path, get_arg))


async def get_response(event, query, path, get_arg, extra_get={}, base=None):
    if not get_arg:
        get = {}
    else:
        get = {
            get_arg: query
        }

    get.update(extra_get)
    url = (base or api_base).join(URL(path)).with_query(get)

    # TODO less hacky way of doing this
    # Forcing IPv4 because IPv6 is borked on the prod server...
    async with aiohttp.client.ClientSession(connector=aiohttp.connector.TCPConnector(family=socket.AF_INET)) as session:
        response = await session.get(url)
        return io.BytesIO(await response.read())


async def get_image(event, query, path, get_arg, extra_get={}, base=None, format="WebP"):
    response = await get_response(event, query, path, get_arg, extra_get, base)
    in_image = Image.open(response)
    return photo_file(in_image, format)


@bot_command("GlaxFlagBot")
async def flag(bot, query: str, event: NewMessageEvent):
    """
    Shows Glax holding one or two flags
    """
    await image_common(event, query, "flag.png", "flag")


@bot_inline("GlaxFlagBot")
async def inline_flag(event: InlineQueryEvent):
    async with InlineHandler(event) as inline:
        if event.query.query:
            inline.sticker(await get_image(event, event.query.query, "flag.png", "flag"))


@bot_command("GlaxTimeBot")
async def time(bot, query: str, event: NewMessageEvent):
    """
    Shows Glax's clock
    """

    await send_sticker(event.client, event.chat, await get_image(event, query, "clock.png", "time"))


@bot_inline("GlaxTimeBot")
async def inline_time(event: InlineQueryEvent):
    async with InlineHandler(event) as inline:
        if event.query.query:
            inline.sticker(await get_image(event, event.query.query, "clock.png", "time"))
            inline.sticker(await get_image(event, event.query.query, "clock.png", "time", {"flip": 1}))
            inline.sticker(await get_image(event, event.query.query, "clock.png", "time", {"sleepy": 1}))
            inline.sticker(await get_image(event, event.query.query, "clock.png", "time", {"flip": 1, "sleepy": 1}))


says_faces = [
    "angry",
    "awoo",
    "bamboozled",
    #"blep",
    #"bored",
    #"clock",
    #"cloud",
    "cyborg",
    "derp",
    "facepalm",
    "fire",
    "hiss",
    "hugs",
    "innocent",
    "laughing",
    "not-sure-if",
    "party",
    "pissed off",
    "pointing",
    #"rawrdical",
    "run",
    "sleepy",
    "smirk",
    "tongue out",
    "triggered",
    "unimpressed",
    "yawn",
]


@bot_inline("GlaxSaysBot")
async def inline_says(event: InlineQueryEvent):
    async with InlineHandler(event) as inline:
        if event.query.query:
            images = await asyncio.gather(*[
                get_image(event, event.query.query, "glax_says.png", "text", {"face": face})
                for face in says_faces
            ])
            for image in images:
                inline.sticker(image)


async def shield_impl(event, query):
    return await get_image(event, query, "drawshield.php", "blazon", {
        "outputformat": "png",
        "palette": "drawshield",
        "shape": "heater",
        "effect": "plain",
        # "size": "512",
        "asfile": "1",
        "webcols": "yes",
        #"tartancols": "yes",
        "customPalette[heraldic/azure]": "#0f47af",
    }, URL("https://drawshield.net/include/"))


@bot_inline("GlaxShieldBot")
async def inline_shield(event: InlineQueryEvent):
    async with InlineHandler(event) as inline:
        if event.query.query:
            inline.sticker(await shield_impl(event, event.query.query))


@bot_command("GlaxShieldBot")
async def shield(bot, query: str, event: NewMessageEvent):
    """
    Renders a shield
    """
    image = await shield_impl(event, query)
    await send_sticker(event.client, event.chat, image)


@bot_command("GlaxWeatherBot")
async def weather(bot, query: str, event: NewMessageEvent):
    """
    Get current weather from the specified location
    """
    data = json.load(await get_response(event, query, "glax_weather.json", "location"))
    image = data["icon"]
    message = data["message"]
    await event.client.send_file(event.chat, image, caption=message)


@bot_inline("GlaxWeatherBot")
async def inline_weather(event: InlineQueryEvent):
    async with InlineHandler(event) as inline:
        if event.query.query:
            data = json.load(await get_response(event, event.query.query, "glax_weather.json", "location"))
            image = data["icon"]
            message = data["message"]
            inline.photo(image, text=message)


@bot_inline("SomeDragonsBot")
async def inline_some_dragons(event: InlineQueryEvent):
    async with InlineHandler(event) as inline:
        inline.photo(await get_image(event, event.query.query, "some_dragons.jpg", "what", format="JPEG"))


@bot_inline("GlaxSealBot")
async def inline_seal(event: InlineQueryEvent):
    async with InlineHandler(event) as inline:
        if event.query.query:
            images = await asyncio.gather(*[
                get_image(event, event.query.query, "glax_seal.png", "text", {"face": face})
                for face in says_faces
            ])
            for image in images:
                inline.sticker(image)


@bot_inline("YouWouldntBot")
async def inline_you_wouldnt(event: InlineQueryEvent):
    async with InlineHandler(event) as inline:
        inline.photo(await get_image(event, event.query.query, "you_wouldnt.jpg", "what", format="JPEG"))


@bot_inline("BrexitBusBot")
async def inline_brexit_bus(event: InlineQueryEvent):
    async with InlineHandler(event) as inline:
        inline.photo(await get_image(event, event.query.query, "brexit_bus.jpg", "text", format="JPEG"))


is_this_pics = [
    "cd",
    "beeps"
]


@bot_inline("IsThisAGlaxBot")
async def inline_is_this(event: InlineQueryEvent):
    async with InlineHandler(event) as inline:
        if event.query.query:
            for who in is_this_pics:
                inline.photo(await get_image(
                    event, event.query.query, "is_this_an_api.jpg", "text", {"who": who, "width": "512"}, format="JPEG"
                ))


@bot_inline("ADRagonBot")
async def inline_adragon(event: InlineQueryEvent):
    async with InlineHandler(event) as inline:
        if event.query.query:
            inline.sticker(await get_image(event, event.query.query, "adragon.png", "text", {"class": "1", "nc": "3"}))
            inline.sticker(await get_image(event, event.query.query, "adragon.png", "text", {"class": "2", "nc": "3"}))
            inline.sticker(await get_image(event, event.query.query, "adragon.png", "text", {"class": "3", "nc": "3"}))
            inline.sticker(await get_image(event, event.query.query, "adragon.png", "text", {"class": "4", "nc": "3"}))
            inline.sticker(await get_image(event, event.query.query, "adragon.png", "text", {"class": "5", "nc": "3"}))
            inline.sticker(await get_image(event, event.query.query, "adragon.png", "text", {"class": "6", "nc": "3"}))


def cert_get_image(event, query):
    title = ""
    text = query.strip()
    if text.startswith("**"):
        title, text = text[2:].split("**", 1)

    return get_image(event, query, "certificate.png", "", {"title": title.strip(), "text": text.strip()})


@bot_inline("GlaxCertBot")
async def inline_certificate(event: InlineQueryEvent):
    async with InlineHandler(event) as inline:
        inline.sticker(await cert_get_image(event, event.query.query))


@bot_command("GlaxCertBot")
async def certificate(bot, query: str, event: NewMessageEvent):
    """
    Renders a Glax certificate sticker
    """
    await send_sticker(event.client, event.chat, await cert_get_image(event, query))
