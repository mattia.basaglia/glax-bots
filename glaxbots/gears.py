import re
import sys
import math
import enum
import copy
import random
from functools import reduce
from fractions import Fraction

from lottie import objects
from lottie import NVector, Color
from lottie.utils import script
from lottie.utils.color import ColorMode
from lottie.nvector import PolarVector
from lottie.utils.ellipse import Ellipse


class Gear(objects.Group):
    def __init__(self, spokes, rotation, color, speed, direction, outer_radius, inner_radius, hole_radius):
        super().__init__()

        self.outer_radius = outer_radius
        self.inner_radius = inner_radius
        self.hole_radius = hole_radius
        self.direction = direction

        self.spokes = spokes
        self.speed = speed
        self.rotation = rotation
        self.color = color
        self.radius = (self.outer_radius + self.inner_radius) / 2
        self.mesh_direction = 1

    @property
    def angle_step(self):
        return math.pi * 2 / self.spokes

    def generic_bezier(self, outer_radius, inner_radius):
        bezier = objects.Bezier()

        for i in range(self.spokes):
            mid_spoke_angle = i * self.angle_step
            spoke_start_angle = mid_spoke_angle - self.angle_step / 4
            spoke_end_angle = mid_spoke_angle + self.angle_step / 4
            gap_end_angle = mid_spoke_angle + 3 / 4 * self.angle_step

            spoke_middle_top = PolarVector(outer_radius, mid_spoke_angle)
            spoke_middle_bottom = PolarVector(inner_radius, mid_spoke_angle)
            spoke_middle_bottom_left = PolarVector(inner_radius, spoke_start_angle)
            spoke_middle_bottom_right = PolarVector(inner_radius, spoke_end_angle)
            mid_left = spoke_middle_bottom - spoke_middle_bottom_left
            mid_right = spoke_middle_bottom - spoke_middle_bottom_right

            bezier.add_point(spoke_middle_top - mid_left)
            bezier.add_point(spoke_middle_top - mid_right)
            bezier.add_point(PolarVector(inner_radius, spoke_end_angle))
            bezier.add_point(PolarVector(inner_radius, gap_end_angle))

        bezier.close()

        return bezier

    def bezier(self):
        return self.generic_bezier(self.outer_radius, self.inner_radius)

    def build(self, speed, last_frame):
        self.add_shape(objects.Path(self.bezier()))

        if self.hole_radius > 0:
            self.add_shape(objects.Ellipse(NVector(0, 0), NVector(self.hole_radius, self.hole_radius) * 2))

        self.add_shape(objects.Stroke(self.color * 0.8, 6))
        self.add_shape(objects.Fill(self.color)).fill_rule = objects.FillRule.EvenOdd

        rot_start = self.rotation / math.pi * 180
        self.transform.rotation.add_keyframe(0, rot_start)
        self.transform.rotation.add_keyframe(last_frame, float(self.direction * self.speed * speed * 360) + rot_start)

    @property
    def position(self) -> NVector:
        return self.transform.position.value

    @position.setter
    def position(self, value: NVector):
        self.transform.position.value = value
        return value

    def bounding_box(self, time=0):
        return objects.BoundingBox(
            self.position.x - self.outer_radius,
            self.position.y - self.outer_radius,
            self.position.x + self.outer_radius,
            self.position.y + self.outer_radius,
        )

    def spokes_moved(self):
        return self.speed * self.spokes

    def spoke_relative_angle(self, direction):
        angle = self.rotation
        for i in range(self.spokes):
            diff = self.radian_diff(direction, angle)
            if diff >= -self.angle_step / 2 and diff <= self.angle_step / 2:
                break
            angle += self.angle_step

        return diff / (self.angle_step / 2)

    @staticmethod
    def radian_diff(angle1, angle2):
        diff = angle2 % (math.pi * 2) - angle1 % (math.pi * 2)
        if diff > math.pi:
            diff -= 2 * math.pi
        elif diff < -math.pi:
            diff += 2 * math.pi
        return diff

    def mesh_rotation(self, other, direction, spokes, mesh_direction):
        rel_angle = other.spoke_relative_angle(direction)

        reverse = True

        if mesh_direction == -1:
            if self.mesh_direction == -1:
                reverse = self.spokes % 2
            elif other.mesh_direction == -1:
                reverse = other.spokes % 2

        if reverse:
            rel_angle = 1 - rel_angle

        angle = math.pi / spokes * rel_angle
        if mesh_direction == -1:
            angle += math.pi
        return angle

    def rotate_to_mesh(self, other, direction):
        self.rotation = self.mesh_rotation(other, direction, self.spokes, self.mesh_direction * other.mesh_direction)
        pos_diff = other.position - self.position
        self.rotation += math.atan2(pos_diff.y, pos_diff.x)

    @property
    def belt_radius(self):
        return self.outer_radius


class DefaultGear(Gear):
    cache = {}

    @staticmethod
    def radii(spokes, spoke_size, radius_step):
        outer_radius = spokes * radius_step + spoke_size / 2
        inner_radius = outer_radius - spoke_size
        hole_radius = inner_radius - spoke_size

        return outer_radius, inner_radius, hole_radius

    def __init__(self, spokes, rotation, color, speed, direction, spoke_size, radius_step):
        super().__init__(spokes, rotation, color, speed, direction, *DefaultGear.radii(spokes, spoke_size, radius_step))

    def bezier(self):
        if self.spokes not in DefaultGear.cache:
            bezier = super().bezier()
            DefaultGear.cache[self.spokes] = bezier
            return bezier

        return DefaultGear.cache[self.spokes]


class RingGear(Gear):
    def __init__(self, spokes, rotation, color, speed, direction, outer_radius, inner_radius, hole_radius):
        super().__init__(spokes, rotation, color, speed, direction, outer_radius, inner_radius, hole_radius)
        self.radius = -self.radius
        self.mesh_direction = -1

    def bounding_box(self, time=0):
        return objects.BoundingBox(
            self.position.x - self.hole_radius,
            self.position.y - self.hole_radius,
            self.position.x + self.hole_radius,
            self.position.y + self.hole_radius,
        )

    #def rotate_to_mesh(self, other, direction):
        #self.rotation = self.mesh_rotation(other, direction, self.spokes, -1)
        #pos_diff = other.position - self.position
        #self.rotation += math.atan2(pos_diff.y, pos_diff.x)

    @property
    def belt_radius(self):
        return self.hole_radius


class Belt(objects.Group):
    bezier_circle_constant = 0.5519

    def __init__(self, belt_input: Gear, belt_output: Gear, default_radius):
        super().__init__()
        self.belt_input = belt_input
        self.belt_output = belt_output
        self.default_radius = default_radius

        self.start_center = self.belt_input.position
        self.start_radius = self.belt_input.belt_radius

        self.end_center = self.belt_output.position
        self.end_radius = self.belt_output.belt_radius
        self.delta_center = self.end_center - self.start_center

        self.speed = self.belt_input.speed * math.pi * 2 * self.start_radius
        self.direction = self.belt_input.direction
        self.segments = (self.belt_input.spokes + self.belt_output.spokes)

        # https://en.wikipedia.org/wiki/Belt_problem
        r1 = self.start_radius
        r2 = self.end_radius
        P = self.delta_center.length
        if r2 > r2:
            r1, r2 = r2, r1
        theta = 2 * math.acos((r1 - r2) / P)
        self.length = 2 * P * math.sin(theta / 2) + r1 * (2 * math.pi - theta) + r2 * theta

        # Tweak speed to always have nice fractions
        speed_precision = 100
        self.length_over_speed = Fraction(self.speed / self.length).limit_denominator(speed_precision)
        if self.length_over_speed.denominator > self.segments:
            self.segments = self.length_over_speed.denominator
        else:
            self.segments = int(round(self.segments / self.length_over_speed.denominator)) * self.length_over_speed.denominator

        self.speed = self.length_over_speed * self.length

    def build(self, speed, last_frame):
        path = objects.Bezier()

        center_angle = math.atan2(self.delta_center.y, self.delta_center.x)
        tangent_angle = math.acos(abs(self.start_radius - self.end_radius) / self.delta_center.length)

        in_ellipse = Ellipse(self.start_center, NVector(self.start_radius, self.start_radius), 0)
        out_ellipse = Ellipse(self.end_center, NVector(self.end_radius, self.end_radius), 0)

        if self.start_radius > self.end_radius:
            for point in in_ellipse.to_bezier(center_angle + tangent_angle,  2 * math.pi - 2 * tangent_angle): # 2nd is large
                path.points.append(point)

            for point in out_ellipse.to_bezier(center_angle - tangent_angle, 2 * tangent_angle):
                path.points.append(point)
        else:
            for point in in_ellipse.to_bezier(center_angle + math.pi - tangent_angle, 2 * tangent_angle):
                path.points.append(point)

            for point in out_ellipse.to_bezier(center_angle + math.pi + tangent_angle, math.pi * 2 - 2 * tangent_angle):
                path.points.append(point)

        path.close()
        path.reverse()

        stroke = objects.Stroke(Color(0, 0, 0.3, mode=ColorMode.HSV).to_rgb(), 6)
        stroke.dashes = [
            objects.StrokeDash(self.length/self.segments/2, objects.StrokeDashType.Dash),
            objects.StrokeDash(self.length/self.segments/2, objects.StrokeDashType.Gap),
            objects.StrokeDash(0, objects.StrokeDashType.Offset),
        ]
        stroke.dashes[-1].length.add_keyframe(0, 0)
        stroke.dashes[-1].length.add_keyframe(last_frame, float(self.direction * self.speed * speed))

        self.add_shape(objects.Path(path))
        self.add_shape(stroke)
        self.add_shape(objects.Stroke(Color(0, 0, 0.8, mode=ColorMode.HSV).to_rgb(), 18))

    def spokes_moved(self):
        return self.length_over_speed * self.segments


class GearParser:
    tokenizer = re.compile(r"[0-9]+(?::[0-9]+)?|[a-z]+[-a-z]*|[-()]")

    directions = {
        "left": math.pi,
        "right": 0,
        "up": -math.pi / 2,
        "down": math.pi / 2,
        "above": -math.pi / 2,
        "below": math.pi / 2,
        "up-right": -math.pi / 4,
        "down-right": math.pi / 4,
        "up-left": math.pi + math.pi / 4,
        "down-left": math.pi - math.pi / 4,
        "hex-up-right": -math.pi / 3,
        "hex-down-right": math.pi / 3,
        "hex-up-left": math.pi + math.pi / 3,
        "hex-down-left": math.pi - math.pi / 3,
    }

    class State:
        def __init__(self):
            self.next_wheel_angle = 0
            self.prev_gear = None
            self.prev_input = None
            self.speed = Fraction(1)
            self.position = NVector(0, 0)
            self.direction = 1

    class Mode(enum.Enum):
        Normal = enum.auto()
        Planet = enum.auto()
        Ring = enum.auto()

    def __init__(self):
        self.stack = []
        self.radius_step = 16
        self.spoke_size = 32

    @property
    def state(self) -> State:
        return self.stack[-1]

    @staticmethod
    def split_tokens(input):
        return GearParser.tokenizer.findall(input)

    def default_gear(self, spokes, rotation, color):
        return DefaultGear(spokes, rotation, color, self.state.speed, self.state.direction, self.spoke_size, self.radius_step)

    def main_gear(self, spokes, color, belt):
        gear = self.default_gear(spokes, 0, color)
        self.make_gear_main(gear, belt)
        self.speed_ratios.append(gear.spokes_moved())

        return gear

    def make_gear_main(self, gear, belt):
        distance = 0

        prev_gear = self.state.prev_gear

        if prev_gear:
            self.state.speed *= Fraction(prev_gear.spokes, gear.spokes)
            distance += prev_gear.radius

        gear.speed = self.state.speed

        if belt and prev_gear:
            distance = (belt + 0.5) * (abs(distance) + abs(gear.radius))
        else:
            distance += gear.radius

        self.state.position += PolarVector(distance, self.state.next_wheel_angle)

        gear.position += self.state.position

        self.state.prev_input = gear

        if prev_gear and not belt:
            next_angle = self.state.next_wheel_angle
            if prev_gear.radius * gear.radius < 0:
                next_angle += math.pi

            gear.rotate_to_mesh(prev_gear, self.state.next_wheel_angle)
            if gear.mesh_direction == prev_gear.mesh_direction:
                gear.direction = -prev_gear.direction
            self.state.direction = gear.direction

        self.state.prev_gear = gear

    def belt(self, belt_input: Gear, belt_output: Gear, gears):
        if belt_input.position == belt_output.position:
            return
        belt = Belt(belt_input, belt_output, self.spoke_size / 2)
        gears.insert_shape(0, belt)
        self.speed_ratios.append(belt.spokes_moved())

    def inner_gear(self, gear, spokes, color, gears):
        gear2 = self.default_gear(spokes, gear.rotation, color)

        self.state.prev_gear = gear2
        #self.state.prev_input = gear2
        self.speed_ratios.append(gear2.spokes_moved())
        gear2.position = gear.position

        large_gear = gear
        small_gear = gear2

        if gear2.spokes > gear.spokes:
            large_gear, small_gear = gear2, gear

        if large_gear.spokes - small_gear.spokes >= 5:
            connector_spokes = math.gcd(gear.spokes, gear2.spokes)
            if connector_spokes == 1:
                connector_spokes = small_gear.spokes

            connector = Gear(
                connector_spokes,
                gear.rotation + math.pi / connector_spokes,
                color,
                gear.speed,
                gear.direction,
                large_gear.inner_radius,
                small_gear.inner_radius,
                small_gear.hole_radius
            )
            self.speed_ratios.append(connector.spokes_moved())
            connector.position = gear.position
        else:
            connector = None

        if gear.spokes == large_gear.spokes:
            if connector:
                gears.insert_shape(0, connector)
            gears.insert_shape(0, large_gear)
            gears.insert_shape(0, small_gear)
        else:
            gears.add_shape(large_gear)
            if connector:
                gears.add_shape(connector)
            gears.insert_shape(0, small_gear)

    def push(self):
        self.stack.append(copy.deepcopy(self.state))

    def pop(self):
        if len(self.stack) > 1:
            self.stack.pop()

    def ring_gear(self, spokes, color, gears, belt):
        outer, inner, hole = DefaultGear.radii(spokes, self.spoke_size, self.radius_step)
        ring = outer + self.spoke_size

        gear = RingGear(spokes, 0, color, self.state.speed, self.state.direction, outer, inner, ring)

        self.make_gear_main(gear, belt)
        self.speed_ratios.append(gear.spokes_moved())

        gears.insert_shape(0, gear)

    def planet_ring_gear(self, sun, spokes, color):
        outer, inner, hole = DefaultGear.radii(spokes, self.spoke_size, self.radius_step)
        ring = outer + self.spoke_size

        speed = sun.speed * sun.spokes / spokes
        gear = RingGear(spokes, sun.rotation, color, speed, -sun.direction, outer, inner, ring)
        gear.position = sun.position

        self.speed_ratios.append(gear.spokes_moved())
        self.state.prev_gear = gear
        if self.state.prev_gear:
            self.state.speed *= Fraction(sun.spokes, spokes)
        self.state.direction = gear.direction

        return gear

    def planet_gear(self, sun, spokes, color, direction):
        gear = self.default_gear(spokes, direction, color)

        gear.speed = sun.speed * sun.spokes / spokes
        gear.direction = -sun.direction
        gear.position = sun.position + PolarVector(sun.radius + gear.radius, direction)
        gear.rotate_to_mesh(sun, direction)

        self.speed_ratios.append(gear.spokes_moved())

        return gear

    def planetary(self, sun_teeth, color, planet_color, gears, n_planets, belt):
        if n_planets is None:
            n_planets = largest_factor(sun_teeth, min(7, sun_teeth // 2 - 1))

        prev_gear = self.state.prev_gear

        min_planet_teeth = 3
        b = int(math.ceil((min_planet_teeth * 2 + sun_teeth) / n_planets))
        ring_teeth = b * n_planets
        planet_teeth = (ring_teeth - sun_teeth) // 2
        #ring_teeth = sun_teeth + 2 * planet_teeth

        sun_gear = self.main_gear(sun_teeth, color, belt)
        ring_gear = self.planet_ring_gear(sun_gear, ring_teeth, color)

        gears.insert_shape(0, sun_gear)
        gears.insert_shape(1, ring_gear)

        if prev_gear:
            planet_pos_direction_start = sun_gear.mesh_rotation(prev_gear, self.state.next_wheel_angle, n_planets, 1)
        else:
            planet_pos_direction_start = 0

        for planet_index in range(n_planets):
            planet_pos_direction = planet_index * math.pi * 2 / n_planets + planet_pos_direction_start
            planet_gear = self.planet_gear(sun_gear, planet_teeth, planet_color, planet_pos_direction)
            if planet_index == 0:
                ring_gear.rotate_to_mesh(planet_gear, planet_pos_direction)
            gears.insert_shape(1, planet_gear)

    def normal_gear(self, spokes, color, spokes2, gears, belt):
        gear = self.main_gear(spokes, color, belt)

        if spokes2:
            self.inner_gear(gear, spokes2, color, gears)
        else:
            gears.insert_shape(0, gear)

    def parse(self, tokens):
        last_frame = 180
        animation = objects.Animation(last_frame)
        layer = animation.add_layer(objects.ShapeLayer())

        parent = objects.Group()
        layer.add_shape(parent)

        belts = objects.Group()
        parent.add_shape(belts)

        gears = objects.Group()
        parent.add_shape(gears)

        start_hue = random.random()

        index = 0
        gear_count = len(list(filter(lambda x: x[0].isdigit(), tokens)))
        self.speed_ratios = []
        first_spokes = 1
        mode = self.Mode.Normal
        belt = 0

        self.stack = [self.State()]

        for token in tokens:
            if token == ")":
                self.pop()
                continue
            elif token == "(":
                self.push()
                continue
            elif token == "-":
                belt += 1
                continue
            elif token in self.directions:
                self.state.next_wheel_angle = self.directions[token]
                continue
            elif token == "planet":
                mode = self.Mode.Planet
            elif token == "ring":
                mode = self.Mode.Ring

            try:
                if ":" in token:
                    spokes, spokes2 = map(int, token.split(":"))

                    if mode != self.Mode.Planet and (spokes2 < 2 or spokes2 == spokes):
                        spokes2 = None

                else:
                    spokes = int(token)
                    spokes2 = None

                if spokes < 2:
                    spokes = 2
            except Exception:
                continue

            if index == 0:
                first_spokes = spokes

            color_hsv = Color((start_hue + index / gear_count) % 1, 0.5, 1, mode=ColorMode.HSV)
            color = color_hsv.to_rgb()

            if belt:
                belt_input = self.state.prev_gear

            if mode == self.Mode.Planet:
                mode = self.Mode.Normal
                planet_color = color_hsv.clone()
                planet_color.h = (planet_color.h + 0.5) % 1
                self.planetary(spokes, color, planet_color.to_rgb(), gears, spokes2, belt)
            elif mode == self.Mode.Ring:
                mode = self.Mode.Normal
                self.ring_gear(spokes, color, gears, belt)
            else:
                self.normal_gear(spokes, color, spokes2, gears, belt)

            if belt and belt_input:
                self.belt(belt_input, self.state.prev_input, belts)
                belt = 0

            index += 1

        speed_mult = lcm(*(x.denominator for x in self.speed_ratios))
        if speed_mult > 2:
            # Try slowing down
            if all((x * speed_mult / first_spokes).denominator == 1 for x in self.speed_ratios):
                speed_mult /= first_spokes

        for gear in gears.shapes[:-1]:
            gear.build(speed_mult, last_frame)

        for belt in belts.shapes[:-1]:
            belt.build(speed_mult, last_frame)

        self.fit_view(parent, animation)

        return animation

    def fit_view(self, shape, animation):
        box = shape.bounding_box()
        if box.width > 0 and box.height > 0:
            center = box.center()
            shape.transform.anchor_point.value = center
            shape.transform.position.value = NVector(animation.width, animation.height) / 2

            pad = self.spoke_size
            scalew = animation.width / (box.width + pad)
            scaleh = animation.height / (box.height + pad)
            scale = min(scalew, scaleh)

            shape.transform.scale.value = NVector(scale, scale) * 100


def lcm(*numbers):
    if sys.version_info >= (3, 9):
        return math.lcm(*numbers)

    prod = reduce(lambda a, b: a * b // math.gcd(a, b), numbers)
    div = reduce(math.gcd, numbers)
    return prod // div


def largest_factor(number, max_factor):
    for i in range(max_factor, 1, -1):
        if number % i == 0:
            return i
    return 1


if __name__ == "__main__":
    parser = script.get_parser(formats=["html"])
    parser.add_argument(
        "text",
    )
    ns = parser.parse_args()

    gear_parser = GearParser()
    tokens = gear_parser.split_tokens(ns.text)
    animation = gear_parser.parse(tokens)
    script.run(animation, ns)
