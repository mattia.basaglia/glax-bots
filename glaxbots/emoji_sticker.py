import sys
import inspect
import pathlib

from lottie.utils.font import EmojiFinder, EmojiFinderFallback

from mini_apps.telegram.auto_bot import bot_command, bot_inline
from mini_apps.telegram.utils import static_sticker_file, send_sticker, InlineHandler
from mini_apps.telegram.events import NewMessageEvent, InlineQueryEvent

from .animations import emoji_finder

emoji_editor_path = pathlib.Path("/var/www/dragon.best/private/tools/emoji_editor")
sys.path.append(str(emoji_editor_path))

fallback_finder = EmojiFinderFallback(
    emoji_editor_path / "assets/twemoji-extras",
    EmojiFinder.basename_twemoji,
    emoji_finder
)


def render_from_query(query, max_images=2):
    from glaxmoji.advanced_renderer import data_from_multi_query
    from glaxmoji.export import image_to_pillow

    image = data_from_multi_query(query, max_images, external_finder=fallback_finder)
    return image_to_pillow(image)


@bot_inline
async def sticker_inline(event: InlineQueryEvent):
    async with InlineHandler(event) as inline:
        inline.webview_button("Customize Sticker", "https://dragon.best/editor/webapp/emoji/")

        if inline.query:
            sticker = render_from_query(inline.query)

            if sticker:
                inline.sticker(static_sticker_file(sticker))


@bot_command
async def start(bot, query: str, event: NewMessageEvent):
    """
    Shows help
    """
    text = inspect.cleandoc("""
    **Glax emoji bot**
    (Description coming soon)
    """)
    await event.client.send_message(event.chat, text, parse_mode='md')


@bot_command
async def help(bot, query: str, event: NewMessageEvent):
    """
    Shows help
    """
    await start(bot, query, event)


@bot_command
async def render(bot, query: str, event: NewMessageEvent):
    """
    Renders a sticker from a share code, emoji, or both
    """
    query = query.strip()

    if not query:
        await event.client.send_message(event.chat, "You need to send me a sticker description with that")
        return

    sticker = render_from_query(query)

    if not sticker:
        await event.client.send_message(event.chat, "Could not generate a sticker from the given description")
        return

    await send_sticker(event.client, event.chat, static_sticker_file(sticker))
