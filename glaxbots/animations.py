import math
import random
import pathlib

from lottie import objects
from lottie.objects import easing
from lottie import NVector, Color
from lottie.utils.font import FontStyle, EmojiFinder
from lottie.utils.stripper import float_strip
from lottie.parsers.svg import parse_svg_file
from lottie.utils import animation as anutils
from lottie.utils.color import ColorMode
from lottie.utils.fancy_text import render_fancy_text
from lottie.utils import funky_parser


from mini_apps.telegram.auto_bot import named_bot_command as bot_command, named_bot_inline as bot_inline
from mini_apps.telegram.utils import animated_sticker_file, send_animated_sticker, InlineHandler
from mini_apps.telegram.bot import TelegramBot
from mini_apps.telegram.events import NewMessageEvent, InlineQueryEvent

from .gears import GearParser


here = pathlib.Path(__file__).absolute().parent
root = here.parent
assets_path = here / "assets" / "animations"
# emoji_path = root / "noto-emoji"
emoji_path = root / "twemoji"
emoji_finder = EmojiFinder.from_path(emoji_path)


async def anim_common(event, query, callback):
    if not query:
        await event.client.send_message(event.chat, "You have to send me some text with that command")
        return

    animation = callback(query)
    await send_animated_sticker(event.client, event.chat, animated_sticker_file(animation))


@bot_command("GlaxMatrixBot")
async def matrix(bot, query: str, event: NewMessageEvent):
    """
    Generate a matrix rain style animation
    """
    await anim_common(event, query, matrix_tgs)


@bot_inline("GlaxMatrixBot")
async def inline_matrix(event: InlineQueryEvent):
    async with InlineHandler(event) as inline:
        if inline.query:
            inline.animated_sticker(animated_sticker_file(matrix_tgs(inline.query)))


def matrix_tgs(text):
    last_frame = 180
    animation = objects.Animation(last_frame)
    offset_time = 5

    font = FontStyle("UbuntuMono", 25.5, emoji_finder=emoji_finder)
    ex = font.ex + 3
    line_height = font.line_height
    n_lines = 0

    def character(ch, parent, time, yoff):
        group = parent.add_shape(font.render(ch).shapes[0])
        for colored in group.find_all((objects.Fill, objects.Stroke)):
            if colored.color.value:
                color = Color.from_color(colored.color.value)
                color.convert(ColorMode.LCH_uv)
                color.hue = 2 * math.pi / 3
                colored.color.value = color.to_color()

        if group.next_x > font.ex:
            scale = font.ex / group.next_x
            group.transform.scale.value *= scale

        group.transform.position.value.y += line_height * yoff
        fill = group.add_shape(objects.Fill())
        color = Color(0, 1, 0)
        fill.color.add_keyframe(time + 0, color)
        fill.color.add_keyframe(time + 40, Color(0, 0, 0), easing.Jump())

        if time != 0:
            group.transform.opacity.add_keyframe(0, 0, easing.Jump())
            group.transform.opacity.add_keyframe(time, 100, easing.Jump())
        group.transform.opacity.add_keyframe(time + 20, 100)
        group.transform.opacity.add_keyframe(time + 80, 0)
        group.add_shape(objects.Stroke(Color(0, 0, 0), 2))

    def add_rain(id, x, y, parent):
        layer = objects.PreCompLayer(id)
        parent.add_layer(layer)
        layer.transform.position.value.x = ex * x
        layer.transform.position.value.y = line_height * y * 4
        start_time = y * offset_time * 4

        layer.start_time = start_time

    def add_line(id, x, off):
        pcl = objects.PreCompLayer(id)
        animation.add_layer(pcl)
        pcl.transform.position.value.x = x * ex
        start_time = last_frame * off
        pcl.start_time = start_time

    def make_column(id, text):
        pc = objects.Precomp(id, animation)
        animation.assets.append(pc)

        layer = pc.add_layer(objects.ShapeLayer())
        for i, c in enumerate(font.renderer.emoji_split(text)):
            character(c, layer, i * offset_time, i + 1)

    n_rows = 6 * 4
    lines = text.splitlines()[:4]
    for line in lines:
        id = "line%s" % n_lines
        n_lines += 1
        line = line[:n_rows]
        if len(line) <= n_rows // 2:
            length = len(line)
            joiner = ""
            if " " in line:
                joiner = " "
                length += 1
            line = joiner.join([line] * (n_rows // length))
        make_column(id, line)

    n_cols = 32
    n_offsets = 16
    off = []
    for i in range(0, n_cols, n_offsets):
        toff = list(range(n_offsets))
        random.shuffle(toff)

        while off and off[-1] in toff[:3]:
            random.shuffle(toff)

        off += toff

    for i in range(n_cols):
        normoff = off[i] / n_offsets
        lineid = "line%s" % random.randint(0, n_lines - 1)
        add_line(lineid, i, normoff)
        add_line(lineid, i, normoff - 1)

    float_strip(animation)
    return animation


class ScremBot(TelegramBot):
    def __init__(self, settings):
        super().__init__(settings)
        self.file = settings["file"]

    @TelegramBot.bot_command
    async def screm(self, query: str, event: NewMessageEvent):
        """
        Make the Glax screm
        """
        await anim_common(event, query, lambda x: screm_tgs(x, -1, self.file))

    @TelegramBot.bot_command
    async def inhale(self, query: str, event: NewMessageEvent):
        """
        Make the Glax inhale some text
        """
        await anim_common(event, query, lambda x: screm_tgs(x, 1, self.file))

    async def on_telegram_inline(self, event: InlineQueryEvent):
        async with InlineHandler(event) as inline:
            text = inline.query.strip()
            inline.animated_sticker(animated_sticker_file(screm_tgs(text+"  ", -1, self.file)))
            inline.animated_sticker(animated_sticker_file(screm_tgs(text+"  ", 1, self.file)))
            inline.animated_sticker(animated_sticker_file(screm_tgs(text, -1, self.file)))
            inline.animated_sticker(animated_sticker_file(screm_tgs(text, 1, self.file)))


_file_cache = {}


def load_svg(file):
    if file in _file_cache:
        return _file_cache[file].clone()

    anim = parse_svg_file(str(assets_path / file), 0, 180)
    _file_cache[file] = anim
    return anim.clone()


def screm_tgs(text, x_scale, file="noises.svg"):
    last_frame = min(40, max(180, len(text)/6*60))
    animation = objects.Animation(last_frame)
    text = text[:16].replace("\n", " ")
    src = load_svg(file)

    above = animation.add_layer(objects.ShapeLayer()).add_shape(src.find("above"))

    marker = next(src.find("marker").find_all(objects.Rect)).bounding_box()
    marker.x2 = 512

    font = FontStyle("Ubuntu:style=italic", marker.height, emoji_finder=emoji_finder)
    fill_color = NVector(1.0, 0.12941176470588237, 0.047058823529411764)
    stroke_color = NVector(0.403921568627451, 0.050980392156862744, 0.01568627450980392)
    stroke_width = 14

    textlayer = animation.add_layer(objects.ShapeLayer())
    container = objects.Group()
    g = render_fancy_text(text, font, fill_color, font.size)
    text_size = NVector(max(1, g.next_x), max(1, g.bounding_box().height))
    scale = min(marker.width / text_size.x, marker.height / text_size.y)
    #scale = 0.5
    g.transform.scale.value *= scale
    g.transform.scale.value.x *= x_scale
    text_size *= scale
    dy = (marker.height - text_size.y) / 2 + text_size.y
    container.add_shape(g)
    repeater = container.add_shape(objects.Repeater())
    repeater.copies.value = 2
    repeater.transform.position.value.x = marker.width
    container.add_shape(objects.Fill(fill_color))
    container.add_shape(objects.Stroke(stroke_color, stroke_width))

    mask = objects.Mask()
    textlayer.masks = [mask]
    clip_rect = objects.Rect()
    clip_rect.position.value = marker.center()
    clip_rect.size.value = NVector(marker.width+stroke_width, marker.height*2+stroke_width)
    if x_scale == -1:
        clip_rect.position.value.x = clip_rect.size.value.x / 2
    mask.shape = clip_rect.to_bezier().shape

    conpos = NVector(marker.x1, marker.y1 + dy)
    if x_scale == -1:
        container.transform.position.add_keyframe(0, conpos + NVector(0, 0))
        container.transform.position.add_keyframe(last_frame, conpos + NVector(marker.width, 0))
    else:
        container.transform.position.add_keyframe(0, conpos + NVector(0, 0))
        container.transform.position.add_keyframe(last_frame, conpos + NVector(-marker.width, 0))
    textlayer.add_shape(container)

    below = animation.add_layer(objects.ShapeLayer()).add_shape(src.find("below"))
    random.seed(1)
    anutils.shake([above.transform.position, below.transform.position],
                  8, 8, 0, last_frame, last_frame/3, easing.Jump())

    if x_scale == -1:
        for layer in animation.layers:
            sh = layer.shapes.pop(0)
            g = layer.insert_shape(0, objects.Group())
            g.add_shape(sh)
            g.transform.scale.value.x *= x_scale
            g.transform.position.value = g.transform.anchor_point.value = NVector(256, 256)

    return animation


@bot_command("GooglaxBot")
async def googlax(bot, query: str, event: NewMessageEvent):
    """
    Generates a Googlax search sticker
    """
    if not event.bot_user.is_admin:
        return
    await anim_common(event, query, googlax_tgs)


@bot_inline("GooglaxBot")
async def googlax_inline(event: InlineQueryEvent):
    async with InlineHandler(event) as inline:
        if not event.bot_user.is_admin:
            return
        inline.animated_sticker(animated_sticker_file(googlax_tgs(inline.query)))


def googlax_tgs(text):
    font = FontStyle('DejaVuSans', 25.5, emoji_finder=emoji_finder)
    text_shapes = []
    maxw = 418
    dx = 0
    for c in font.renderer.emoji_split(text):
        sh = font.render(c)
        dx += sh.next_x
        if dx > maxw:
            break
        text_shapes.append(sh)

    text_time = min(180, len(text_shapes)*6)
    char_time = text_time / (len(text_shapes) + 1)
    text_start = min(60, (180-text_time)/2)
    last_frame = min(180, text_start*2 + text_time)
    animation = parse_svg_file(assets_path / "googlax.svg", 0, last_frame)

    g = objects.Group()
    animation.layers[0].insert_shape(0, g)

    cursor = animation.find("path4625")
    curbb = cursor.bounding_box()
    g.transform.position.value = NVector(curbb.x1, curbb.y2)

    cursor.transform.position.add_keyframe(0, NVector(0, 0), easing.Jump())

    dx = 0
    for i, sh in enumerate(text_shapes):
        g.add_shape(sh)
        sh.transform.position.value.x = dx
        ntime = text_start + char_time * (i+1)
        dx += sh.next_x
        sh.add_shape(objects.Fill(NVector(0, 0, 0)))
        sh.transform.opacity.add_keyframe(0, 0, easing.Jump())
        sh.transform.opacity.add_keyframe(ntime, 100, easing.Jump())
        cursor.transform.position.add_keyframe(ntime, NVector(dx+5, 0), easing.Jump())

    for i in range(0, last_frame, 30):
        cursor.transform.opacity.add_keyframe(i, 0, easing.Jump())
        cursor.transform.opacity.add_keyframe(i+10, 100, easing.Jump())

    return animation


def think_tgs(text, file="think.svg"):
    last_frame = 180
    animation = load_svg(file)

    pop_time = 20
    pop_off = 15
    think_start = 0
    think_end = think_start + pop_off * 7

    thought_1 = animation.find("thought 1")
    thought_2 = animation.find("thought 2")
    thought_3a = animation.find("thought 3 a")
    thought_3b = animation.find("thought 3 b")

    text_marker = next(animation.find("markers").find_all(objects.Rect)).bounding_box()

    text_layer = animation.find("text")

    font_size = text_marker.height
    font = FontStyle("Ubuntu:style=italic", font_size, emoji_finder=emoji_finder)
    fill_color = NVector(1.0, 0.12941176470588237, 0.047058823529411764)
    stroke_color = NVector(0.202, 0.025, 0.008)
    stroke_width = 1

    pos = NVector(0, 0)
    g = render_fancy_text(text, font, fill_color, font_size, pos)
    text_size = NVector(pos.x, g.bounding_box().height)

    if text_size.x != 0 and text_size.y != 0:
        scale = min(text_marker.width / text_size.x, text_marker.height / text_size.y)
        g.transform.scale.value *= scale
        text_bounds = g.bounding_box()
        container = objects.Group()
        container.add_shape(g)
        container.transform.position.value = text_marker.center() - text_bounds.center()
        #container.add_shape(objects.Fill(fill_color))
        stroke = container.add_shape(objects.Stroke(stroke_color, stroke_width))
        stroke.width.add_keyframe(think_start + pop_off * 3.5, 0)
        stroke.width.add_keyframe(think_start + pop_off * 4.5, stroke_width)
        stroke.width.add_keyframe(last_frame - pop_time - pop_off, stroke_width)
        stroke.width.add_keyframe(last_frame - pop_time, 0)
        text_layer.add_shape(container)

    def pop_in(time, group, out_time):
        try:
            stroke = next(group.find_all(objects.Stroke))
            width = stroke.width.value
            stroke.width.add_keyframe(0, 0)
            stroke.width.add_keyframe(time, 0)
            stroke.width.add_keyframe(time+5, width)
            stroke.width.add_keyframe(out_time, width)
            stroke.width.add_keyframe(out_time + pop_time, 0)
        except StopIteration:
            pass

        object = next(group.find_all(objects.Ellipse))
        dest = object.size.value
        object.size.add_keyframe(0, NVector(0, 0))
        anutils.spring_pull(object.size, dest, time, time+pop_time)
        object.size.add_keyframe(out_time, dest)
        object.size.add_keyframe(out_time + pop_time, NVector(0, 0))

    for e in thought_1.find_all(objects.Group, None, False):
        pop_in(think_start, e, think_start + pop_time * 2 + pop_off)

    for e in thought_2.find_all(objects.Group, None, False):
        pop_in(think_start+pop_off, e, think_start + pop_time * 2 + pop_off * 2)

    bubb = list(zip(thought_3a.shapes[:-1], thought_3b.shapes[:-1]))
    random.shuffle(bubb)

    for i, (a, b) in enumerate(bubb):
        time = think_start + pop_off * 2 + (i / len(bubb)) * pop_time
        out_time = think_end + (i / len(bubb)) * pop_time
        pop_in(time, a, out_time)
        pop_in(time, b, out_time)

    text_layer.transform.opacity.add_keyframe(think_start + pop_off * 3, 0)
    text_layer.transform.opacity.add_keyframe(think_start + pop_off * 5, 100)
    text_layer.transform.opacity.add_keyframe(last_frame - pop_time, 100)
    text_layer.transform.opacity.add_keyframe(last_frame, 0)

    return animation


@bot_command("GlaxThinkBot")
async def think(bot, query: str, event: NewMessageEvent):
    """
    Make Glax think
    """
    await anim_common(event, query, lambda x: think_tgs(x))


@bot_inline("GlaxThinkBot")
async def think_inline(event: InlineQueryEvent):
    async with InlineHandler(event) as inline:
        text = inline.query.strip()
        inline.animated_sticker(animated_sticker_file(think_tgs(text)))


@bot_command("GlaxThinkBot", trigger="start")
async def think_start(bot, query: str, event: NewMessageEvent):
    """
    Show help
    """
    text = """You can use /think here or as an inline bot in any chat

**Formatting**

Formatting commands affect all following text:

`\\color{`__color__`}`
Change the color to the given argument (or default color if missing), any valid CSS colors should be accepted

`\\huge` `\\large` `\\normal` `\\small` `\\tiny`
Change text size (relative to normal text)

`\\super` `\\sub` `\\center`
Change text y position (can be stacked)

`\\flip`
Mirror the following text block along the x axis

`\\yflip`
Mirror the following text block along the y axis

`\\r`
Go back to the start of the line

`\\n`
New line

`\\hspace{`__amount__`}`
Move horizontally based on the given number

`\\rot{`__angle__`}`
Sets the rotation angle in degrees

`\\clear`
Reset to default formatting"""

    await event.client.send_message(event.chat, text, parse_mode='md')


class FunkyParserData:
    def __init__(self):
        self.font = FontStyle("Ubuntu", 80, emoji_finder=emoji_finder)

        svg_loader = funky_parser.SvgLoader()
        image_root = pathlib.Path("/var/www/dragon.best/media/img/vectors/")
        common_features = {
            "scales": funky_parser.SvgFeature([], ["#3250b0", "#292f75"]),
            "belly": funky_parser.SvgFeature([], ["#c4d9f5"]),
            "eyes": funky_parser.SvgFeature([], ["#f01d0a"]),
            "horns": funky_parser.SvgFeature([], ["#3a3c3f"]),
        }

        self.svg_shapes = [
            funky_parser.SvgShape(
                str(image_root / "blep.svg"),
                ["glax", "blep"],
                common_features,
                "scales",
                1,
                svg_loader
            ),
            funky_parser.SvgShape(
                str(image_root / "huff.svg"),
                ["huffing", "glax"],
                common_features,
                "scales",
                -1,
                svg_loader
            ),
            funky_parser.SvgShape(
                str(image_root / "anguish.svg"),
                ["scared", "glax"],
                common_features,
                "scales",
                -1,
                svg_loader
            ),
            funky_parser.SvgShape(
                str(image_root / "clock.svg"),
                ["glax", "on", "a", "clock"],
                common_features,
                "scales",
                -1,
                svg_loader
            ),
            funky_parser.SvgShape(
                str(image_root / "cooking.svg"),
                ["cooking", "glax"],
                common_features,
                "scales",
                1,
                svg_loader
            ),
            funky_parser.SvgShape(
                str(image_root / "facepalm.svg"),
                ["glax", "facepalm"],
                common_features,
                "scales",
                -1,
                svg_loader
            ),
            funky_parser.SvgShape(
                str(image_root / "hatching.svg"),
                ["baby", "glax"],
                common_features,
                "scales",
                -1,
                svg_loader
            ),
            funky_parser.SvgShape(
                str(image_root / "innocent.svg"),
                ["innocent", "glax"],
                common_features,
                "scales",
                1,
                svg_loader
            ),
            funky_parser.SvgShape(
                str(image_root / "party.svg"),
                ["party", "glax"],
                common_features,
                "scales",
                -1,
                svg_loader
            ),
            funky_parser.SvgShape(
                str(image_root / "pissed off.svg"),
                ["angry", "glax"],
                common_features,
                "scales",
                1,
                svg_loader
            ),
            funky_parser.SvgShape(
                str(image_root / "sitting.svg"),
                ["sitting", "glax"],
                common_features,
                "scales",
                1,
                svg_loader
            ),
            funky_parser.SvgShape(
                str(image_root / "box.svg"),
                ["glax", "in", "a", "box"],
                common_features,
                "scales",
                1,
                svg_loader
            ),
            funky_parser.SvgShape(
                str(image_root / "freezing.svg"),
                ["freezing", "glax"],
                common_features,
                "scales",
                -1,
                svg_loader
            ),
            funky_parser.SvgShape(
                str(image_root / "hiss.svg"),
                ["hissing", "glax"],
                common_features,
                "scales",
                -1,
                svg_loader
            ),
            funky_parser.SvgShape(
                str(image_root / "laughing.svg"),
                ["laughing", "glax"],
                common_features,
                "scales",
                1,
                svg_loader
            ),
            funky_parser.SvgShape(
                str(image_root / "melting.svg"),
                ["melting", "glax"],
                common_features,
                "scales",
                -1,
                svg_loader
            ),
            funky_parser.SvgShape(
                str(image_root / "pizza.svg"),
                ["glax", "eating", "pizza"],
                common_features,
                "scales",
                -1,
                svg_loader
            ),
            funky_parser.SvgShape(
                str(image_root / "sick.svg"),
                ["sick", "glax"],
                common_features,
                "scales",
                0,
                svg_loader
            ),
            funky_parser.SvgShape(
                str(image_root / "sleepy.svg"),
                ["sleepy", "glax"],
                common_features,
                "scales",
                -1,
                svg_loader
            ),
            funky_parser.SvgShape(
                str(image_root / "yawn.svg"),
                ["yawning", "glax"],
                common_features,
                "scales",
                1,
                svg_loader
            ),
            funky_parser.SvgShape(
                str(image_root / "bamboozled.svg"),
                ["surprised", "glax"],
                common_features,
                "scales",
                -1,
                svg_loader
            ),
            funky_parser.SvgShape(
                str(image_root / "blargh.svg"),
                ["glax", "puking", "rainbows"],
                common_features,
                "scales",
                1,
                svg_loader
            ),
            funky_parser.SvgShape(
                str(image_root / "bored.svg"),
                ["bored", "glax"],
                common_features,
                "scales",
                -1,
                svg_loader
            ),
            funky_parser.SvgShape(
                str(image_root / "fall.svg"),
                ["fallen", "glax"],
                common_features,
                "scales",
                1,
                svg_loader
            ),
            funky_parser.SvgShape(
                str(image_root / "flying.svg"),
                ["flying", "glax"],
                common_features,
                "scales",
                1,
                svg_loader
            ),
            funky_parser.SvgShape(
                str(image_root / "phone.svg"),
                ["texting", "glax"],
                common_features,
                "scales",
                -1,
                svg_loader
            ),
            funky_parser.SvgShape(
                str(image_root / "smirk.svg"),
                ["smirking", "glax"],
                common_features,
                "scales",
                -1,
                svg_loader
            ),
            funky_parser.SvgShape(
                str(image_root / "tongue out.svg"),
                ["winking", "glax"],
                common_features,
                "scales",
                -1,
                svg_loader
            ),
            funky_parser.SvgShape(
                str(image_root / "unimpressed.svg"),
                ["unimpressed", "glax"],
                common_features,
                "scales",
                -1,
                svg_loader
            ),
            funky_parser.SvgShape(
                str(image_root / "thumbs-up.svg"),
                ["glax", "giving", "the", "thumbs", "up"],
                common_features,
                "scales",
                -1,
                svg_loader
            ),
        ]

    def parser(self, text, logger):
        parser = funky_parser.Parser(text, funky_parser.DummyLogger())
        parser.allow_resize = False
        parser.max_duration = 180
        parser.svg_shapes = self.svg_shapes
        parser.font = self.font
        return parser


funky_parser_data = FunkyParserData()


@bot_inline("StickerFromDescriptionBot")
async def description_inline(event: InlineQueryEvent):
    async with InlineHandler(event) as inline:
        text = inline.query.strip()
        parser = funky_parser_data.parser(text, funky_parser.DummyLogger())
        animation = parser.parse()
        inline.animated_sticker(animated_sticker_file(animation))


@bot_command("StickerFromDescriptionBot")
async def sticker(bot, query: str, event: NewMessageEvent):
    """
    Build a sticker from a textual description
    """
    if not event.query:
        await event.client.send_message(event.chat, "You have to send me some text with that command")
        return

    logger = funky_parser.StorageLogger()
    parser = funky_parser_data.parser(event.query, logger)
    animation = parser.parse()
    await send_animated_sticker(event.client, event.chat, animated_sticker_file(animation))
    if logger.messages:
        await event.client.send_message(event.chat, "\n".join(logger.messages))


@bot_command("GlaxGearBot")
async def gears(bot, query: str, event: NewMessageEvent):
    """
    Create some gears
    """
    if not query:
        await event.client.send_message(event.chat, "You have to send me some text with that command")
        return

    tokens = GearParser.split_tokens(query)

    if not tokens:
        await event.client.send_message(event.chat, "You must send me a sequence of teeth or ratios, try: 4 5 6")
        return

    parser = GearParser()
    animation = parser.parse(tokens)
    await send_animated_sticker(event.client, event.chat, animated_sticker_file(animation))


@bot_inline("GlaxGearBot")
async def gears_inline(event: InlineQueryEvent):
    async with InlineHandler(event) as inline:
        tokens = GearParser.split_tokens(inline.query)
        if not tokens:
            tokens = ["12"]

        parser = GearParser()
        animation = parser.parse(tokens)
        inline.animated_sticker(animated_sticker_file(animation))


@bot_command("GlaxGearBot", trigger="start")
async def gear_start(bot, query: str, event: NewMessageEvent):
    """
    Show help
    """
    text = """You can use /gears here or as an inline bot in any chat

For the most basic usage, you specify a sequence of gears as their number of teeth:

`4 5 6`

This adds a new gear with the given teeth to the right of the previous gear.

You can also specify a gear as a ratio:

`4 12:5 6`

This will create a wheel with 12 teeth connected to the 4-wheel,
and inside it, it has a 5 spoke wheel. The 6-wheel is connected to the inner wheel.

You can specify the direction following wheels will attach to:

`4 down 5 6 left 5`

Valid directions are:

%s

You can have branching paths by using brackets:

`4 (up 5) right 6`

Here the 5-wheel will be above the 4, and the 6 to the right of 4.

You can also have planetary gears:

`planet 5`

The number represents the number of teeth in the "sun" gear.

You can also specify the number of planets:

`planet 12:6`


And similarly, you can specify ring gears (gears with teeth on the inside):

`ring 12`

You can have gears joined by belts using `-`, the more dashes, the longer the belt.


`3 -- 6 8 6 - 12`

""" % ("\n".join("- " + key for key in GearParser.directions.keys()))

    await event.client.send_message(event.chat, text, parse_mode='md')
