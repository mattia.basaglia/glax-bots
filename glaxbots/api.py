from nltk.sentiment import vader
import nltk

from aiohttp.web import json_response
from mini_apps.http.web_app import WebApp, view


class SentimentAnalysis:
    _instance = None

    @classmethod
    def instance(cls):
        if not cls._instance:
            cls._instance = cls()

        return cls._instance

    def download(self):
        nltk.download("punkt")
        nltk.download("vader_lexicon")

    def __init__(self):
        self.tokenizer = nltk.data.load("tokenizers/punkt/english.pickle")
        self.analyzer = vader.SentimentIntensityAnalyzer()
        self.analyzer.lexicon.update({
            "dragon": 1.0,
            "dragons": 1.0,
            "derg": 1.0,
            "dergs": 1.0,
            "stimky": -2.0,
            "rawr": 1.0,
            "sex": 1.0,
            "lewd": 1.0,
            "glax": 1.0,
        })

    def polarity_score(self, sentence):
        return self.analyzer.polarity_scores(sentence)["compound"]

    def api_polarity_score(self, sentence):
        return {
            "text": sentence,
            "score": self.polarity_score(sentence)
        }

    def paragraph_score(self, paragraph):
        if paragraph == "":
            return {"global": {"text": "", "score": 0}, "sentences": [{"text": "", "score": 0}]}

        scores = []

        for sentence in self.tokenizer.tokenize(paragraph):
            scores.append(self.api_polarity_score(sentence))

        return {
            "global": self.api_polarity_score(paragraph),
            "sentences": scores
        }


class Api(WebApp):
    @view
    async def sentiment(self, request):
        query = request.url.query.get("q", "")
        headers = {}
        headers["Access-Control-Allow-Origin"] = "*"
        headers["Access-Control-Allow-Methods"] = "GET, OPTIONS"
        response = json_response(SentimentAnalysis.instance().paragraph_score(query), headers=headers)
        return response
