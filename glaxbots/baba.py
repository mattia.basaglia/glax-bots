#!/usr/bin/env python3
import math
import json
import enum
import random
import pathlib

import grapheme

import lottie
from lottie.parsers.svg.importer import parse_color
from lottie.utils import script


class Direction(enum.Enum):
    static = -1
    left = 180
    right = 0
    up = 270
    down = 90

    def to_delta(self):
        if self == Direction.left:
            return (-1, 0)
        elif self == Direction.right:
            return (1, 0)
        elif self == Direction.down:
            return (0, 1)
        elif self == Direction.up:
            return (0, -1)
        else:
            return (0, 0)

    def step(self, x, y):
        dx, dy = self.to_delta()
        return x+dx, y+dy


class PictureType(enum.Enum):
    word = enum.auto()
    static = enum.auto()
    directional = enum.auto()
    tiled = enum.auto()


class BabaObjectType(enum.Enum):
    Word = enum.auto()
    BackgroundWord = enum.auto()
    Picture = enum.auto()


class BabaObject:
    def __init__(self, word: str, type: BabaObjectType, data: dict, props=[]):
        self.type = type
        self.word = word

        if self.type == BabaObjectType.Picture and "picture" not in data:
            self.type = BabaObjectType.Word

        self.data = data
        self.default_color = self.data.get("color", None)
        self.picture_direction = Direction.static
        self.direction = Direction.static
        self.file = None
        self.color = None
        self.picture_type = PictureType.word
        #self.default_properties = data.get("properties", [])
        self.set_properties(props)

    @property
    def is_word(self):
        return self.type != BabaObjectType.Picture

    def set_properties(self, properties, all_is_color=False):
        self.color = None

        for property in properties:
            lower = property.lower()
            if lower in Direction.__members__.keys():
                self.direction = Direction[lower]
            elif all_is_color or property in BabaData.default_colors:
                self.color = property

        self.properties = set(properties)

    def get_picture(self, map, x, y):
        if self.type == BabaObjectType.Picture:
            picture_data = self.data["picture"]
            self.picture_type = PictureType[picture_data["type"]]

            picture = picture_data["default"]

            if self.picture_type == PictureType.directional:
                direction_name = self.direction.name
                if direction_name in picture_data:
                    picture = direction_name
                elif direction_name == "left" and "right" in picture_data:
                    picture = "right"
                elif direction_name == "right" and "left" in picture_data:
                    picture = "left"
                elif direction_name == "down" and "up" in picture_data:
                    picture = "up"
                elif direction_name == "up" and "down" in picture_data:
                    picture = "down"

                self.picture_direction = Direction[picture]
            elif self.picture_type == PictureType.tiled:
                picture = ""
                if map.has_picture(self.word, x, y+1):
                    picture += "d"
                if map.has_picture(self.word, x-1, y):
                    picture += "l"
                if map.has_picture(self.word, x, y-1):
                    picture += "u"
                if map.has_picture(self.word, x+1, y):
                    picture += "r"

                if picture == "":
                    picture = "single"

            if isinstance(picture, str):
                self.file = picture_data[picture]["file"]
            else:
                self.file = picture["file"]

        else:
            if "text" in self.data:
                self.file = self.data["text"]["file"]

        if self.picture_type != PictureType.directional:
            self.direction = Direction.static

        self._build_key()

    def _build_key(self):
        self.cache_key = self.word

        if self.type == BabaObjectType.Picture:
            if isinstance(self.file, str):
                self.cache_key = self.file
            else:
                self.cache_key = str(id(self.file))
        elif self.type == BabaObjectType.BackgroundWord:
            self.cache_key += " background"

        self.raw_cache_key = self.cache_key

        if self.picture_direction != self.direction:
            self.cache_key += " " + self.direction.name

        if self.color:
            self.cache_key += " " + self.color

    def apply_transform(self, precomp):
        if self.type == PictureType.tiled:
            scale = lottie.NVector(102, 102)
            for layer in precomp.layers:
                layer.transform.scale.value = scale
            return

        if (
            self.direction == Direction.static or
            self.picture_direction == Direction.static or
            self.direction == self.picture_direction
        ):
            return

        delta = (self.direction.value - self.picture_direction.value) % 360
        if delta == 180:
            if self.direction == Direction.up or self.direction == Direction.down:
                scale = lottie.NVector(100, -100)
            else:
                scale = lottie.NVector(-100, 100)

            for layer in precomp.layers:
                layer.transform.scale.value = scale
        else:
            for layer in precomp.layers:
                layer.transform.rotation.value = delta

    def to_word(self, x, y, horizontal, vertical):
        return Word(self.word, self.word_type(), x, y, horizontal, vertical)

    def word_type(self):
        try:
            return WordType[self.data["word_type"]]
        except KeyError:
            return WordType.noun

    def to_data(self):
        data = self.word
        if self.type == BabaObjectType.Picture:
            data += "!"

        data += ":"

        if self.properties:
            data += ",".join(self.properties)

        data += "," + self.direction.name.upper()

        return data


class WordType(enum.Enum):
    character = enum.auto()
    noun = enum.auto()
    verb = enum.auto()
    AND = enum.auto()
    NOT = enum.auto()
    prefix_condition = enum.auto()
    infix_condition = enum.auto()
    property = enum.auto()


class Word:
    def __init__(self, word, type, x, y, horizontal, vertical):
        self.word = word
        self.type = type
        self.x = x
        self.y = y
        self.vertical = vertical
        self.horizontal = horizontal

    def __str__(self):
        return self.word

    def __repr__(self):
        return "<%s %r>" % (self.__class__.__name__, self.word)


class AstNode:
    @classmethod
    def _to_string(cls, word, negated):
        if negated:
            return "NOT " + word
        return word

    @classmethod
    def _list_to_string(cls, words):
        return " & ".join(map(str, words))

    def __repr__(self):
        return "<%s %s>" % (self.__class__.__name__, self)


class AstPrefix(AstNode):
    def __init__(self, condition, negated):
        self.condition = condition
        self.negated = negated

    def __str__(self):
        return self._to_string(self.condition, self.negated)

    def matches(self, instance: "Instance"):
        result = False

        if self.condition == "LONELY":
            result = len(instance.with_words) == 0
        elif self.condition == "IDLE":
            result = instance.map.direction == Direction.static
        elif self.condition == "POWERED":
            result = instance.map.powered
        elif self.condition == "OFTEN":
            result = random.random() < 3/4
        elif self.condition == "SELDOM":
            result = random.random() < 1/6
        else:
            return False

        if self.negated:
            result = not result

        return result


class AstObject(AstNode):
    def __init__(self, word, negated):
        self.word = word
        self.negated = negated

    def __str__(self):
        return self._to_string(self.word, self.negated)

    def matches(self, word):
        result = False
        if word == self.word:
            result = True
        if self.negated:
            result = not result
        return result

    def matches_any(self, words):
        if self.negated:
            return words != {self.word}
        else:
            return self.word in words


class AstNoun(AstObject):
    pass


class AstAll(AstNoun):
    def matches(self, word):
        return not self.negated

    def matches_any(self, words):
        return not self.negated and len(words)


class AstProperty(AstObject):
    pass


class AstInfix(AstNode):
    def __init__(self, condition, arguments, negated):
        self.condition = condition
        self.arguments = arguments
        self.negated = negated
        self.nouns = set()
        self.properties = set()

        for arg in self.arguments:
            if isinstance(arg, AstNoun):
                self.nouns.add(arg)
            elif isinstance(arg, AstProperty):
                self.properties.add(arg)

    def __str__(self):
        str = self.condition + " " + self._list_to_string(self.arguments)
        return self._to_string(str, self.negated)

    def matches(self, instance: "Instance"):
        result = False

        if self.condition == "WITHOUT":
            result = True
            all_objects = set(other.word for other in instance.map.instances)
            for noun in self.nouns:
                if noun.matches_any(all_objects):
                    result = False
                    break
        elif self.condition == "ABOVE":
            result = self._near(instance.map, instance.x, instance.y+1)
        elif self.condition == "BELOW":
            result = self._near(instance.map, instance.x, instance.y-1)
        elif self.condition == "BESIDELEFT":
            result = self._near(instance.map, instance.x-1, instance.y)
        elif self.condition == "BESIDERIGHT":
            result = self._near(instance.map, instance.x+1, instance.y)
        elif self.condition == "FEELING":
            result = len(self.properties) > 0
            for prop in self.properties:
                if not prop.matches_any(instance.object.properties):
                    result = False
                    break
        elif self.condition == "ON":
            result = True
            for noun in self.nouns:
                if not noun.matches_any(instance.with_words):
                    result = False
                    break
        elif self.condition == "NEAR":
            result = (
                self._near(instance.map, instance.x, instance.y+1) or
                self._near(instance.map, instance.x, instance.y-1) or
                self._near(instance.map, instance.x+1, instance.y) or
                self._near(instance.map, instance.x-1, instance.y) or
                self._near(instance.map, instance.x+1, instance.y+1) or
                self._near(instance.map, instance.x+1, instance.y-1) or
                self._near(instance.map, instance.x-1, instance.y+1) or
                self._near(instance.map, instance.x-1, instance.y-1)
            )
        elif self.condition == "NEXTTO":
            # TODO missing image in data
            result = (
                self._near(instance.map, instance.x, instance.y+1) or
                self._near(instance.map, instance.x, instance.y-1) or
                self._near(instance.map, instance.x+1, instance.y) or
                self._near(instance.map, instance.x-1, instance.y)
            )
        elif self.condition == "FACING":
            dir = instance.direction
            if dir == Direction.down:
                coord = (instance.x, instance.y+1)
            elif dir == Direction.up:
                coord = (instance.x, instance.y-1)
            elif dir == Direction.left:
                coord = (instance.x-1, instance.y)
            else:
                coord = (instance.x+1, instance.y)
            result = self._near(instance.map, *coord)
        else:
            return False

        if self.negated:
            result = not result

        return result

    def _near(self, map, x, y):
        if not self.nouns:
            return False

        for noun in self.nouns:
            if not map.has_object(noun.word, x, y):
                return False
        return True


class Sentence(AstNode):
    def __init__(self, prefix_conditions, subject, infix_conditions, verb, object):
        self.prefix_conditions = prefix_conditions
        self.subject = subject
        self.infix_conditions = infix_conditions
        self.verb = verb
        self.object = object

    def to_singles(self):
        copies = []
        for subject in self.subject:
            for object in self.object:
                copies.append(Sentence(self.prefix_conditions, subject, self.infix_conditions, self.verb, object))
        return copies

    def __str__(self):
        ret = ""

        if self.prefix_conditions:
            ret += self._list_to_string(self.prefix_conditions)
            ret += " "

        ret += str(self.subject)
        ret += " "

        if self.infix_conditions:
            ret += self._list_to_string(self.infix_conditions)
            ret += " "

        ret += self.verb
        ret += " "

        ret += str(self.object)

        return ret

    def applies_to(self, instance):
        if not self.subject.matches(instance.word):
            return False

        for cond in self.prefix_conditions:
            if not cond.matches(instance):
                return False

        for cond in self.infix_conditions:
            if not cond.matches(instance):
                return False

        return True


class SentenceCompiler:
    def compile(self, words):
        # Collapse multiple negations
        new_words = []
        index = 0
        while index < len(words):
            if words[index].word == "NOT":
                not_count = 0
                while index+not_count < len(words) and words[index+not_count].word == "NOT":
                    not_count += 1
                if not_count % 2:
                    new_words.append(words[index])
                index += not_count
            else:
                new_words.append(words[index])
                index += 1

        self.words = new_words
        self.index = 0
        prefixes = self._gather({WordType.prefix_condition})
        nouns = self._gather({WordType.noun})
        infixes = self._infixes()
        if self.word.type != WordType.verb:
            raise TypeError
        verb = self.word.word
        self.next_word()
        objects = self._gather({WordType.noun, WordType.property})
        ast = Sentence(prefixes, nouns, infixes, verb, objects)
        return ast.to_singles()

    def next_word(self):
        self.index += 1

    @property
    def word(self):
        return self.words[self.index]

    def _gather(self, types):
        values = []

        while self.index < len(self.words):
            if self.word.type == WordType.NOT:
                self.next_word()
                if self.word.type not in types:
                    self.index -= 1
                    break
                else:
                    values.append(self._gather_single(self.word, True))
                    self.next_word()
            elif self.word.type in types:
                values.append(self._gather_single(self.word, False))
                self.next_word()
            elif self.word.type == WordType.AND:
                self.next_word()
            else:
                break

        return values

    def _gather_single(self, word, negated):
        if word.type == WordType.noun:
            if word.word == "ALL":
                return AstAll(word.word, negated)
            return AstNoun(word.word, negated)
        elif word.type == WordType.prefix_condition:
            return AstPrefix(word.word, negated)
        elif word.type == WordType.property:
            return AstProperty(word.word, negated)

    def _infixes(self):
        infixes = []
        negated = False

        while self.index < len(self.words):
            if self.word.type == WordType.AND:
                self.next_word()
            elif self.word.type == WordType.NOT:
                negated = True
                self.next_word()
            elif self.word.type == WordType.infix_condition:
                cond = self.word.word
                self.next_word()
                what = self._gather({WordType.noun, WordType.property})
                infixes.append(AstInfix(cond, what, negated))
                negated = False
            elif self.word.type == WordType.verb:
                break
            else:
                raise TypeError

        return infixes


class TooComplex(Exception):
    pass


class SyntaxParser:
    """
    Graph from https://github.com/PlasmaFlare/baba-modding-guide/blob/master/references/parsing.md#part-a
    Approximated grammar:
    SENTENCE := OPT_PREFIX NOUN_LIST OPT_INFIX VERB OBJECT
    OPT_PREFIX := e | NOT_PREFIX PREFIX_AND
    NOT_PREFIX := prefix | not PREFIX
    PREFIX_AND := e | and NOT_PREFIX PREFIX_AND
    NOUN_LIST := NOUN | NOUN and NOUN_LIST
    OPT_INFIX := e | INFIX_LIST
    INFIX_LIST := infix PROP_NOUN_LIST
    PROP_NOUN_LIST := PROP_NOUN | PROP_NOUN and PROP_NOUN_LIST
    PROP_NOUN := NOUN | PROPERTY
    NOUN := noun | not NOUN
    PROPERTY := property | not PROPERTY
    VERB := verb
    OBJECT := PROP_NOUN_LIST
    """
    class Fail(Exception):
        pass

    def __init__(self, sentence):
        self.sentence = sentence
        self.cleaned = []
        self._reset(0)

    def _reset(self, start_index):
        self.index = start_index
        self.success_index = start_index
        self.prev_tile_type = None
        self.prev_state = None
        self.stage_2 = False
        self.stage_3 = False
        self.verb_got = False
        self.no_conds = False
        self.doing_cond = False
        self.start_index = start_index
        self.try_again = start_index

    def clean(self):
        try:
            self._graph_0()
        except self.Fail:
            self._end()

    def next_word(self, first_not=True):
        if self.index >= len(self.sentence):
            raise self.Fail()

        self.prev_tile_type = self.sentence[self.index-1] if self.index > 0 else None
        word = self.sentence[self.index]
        self.index += 1

        if first_not and word.type in (WordType.NOT, WordType.noun, WordType.prefix_condition):
            self.try_again = self.index - 1

        if word.type == WordType.NOT:
            return self.next_word(False)
        return word

    def _transition(self, state_from, state_to):
        self.prev_state = state_from
        if not state_to:
            raise self.Fail()
        state_to()

    def _graph_0(self):
        word = self.next_word()

        if word.type == WordType.prefix_condition:
            next = self._graph_1
        elif word.type == WordType.noun:
            next = self._graph_2
        else:
            next = None

        self._transition(self._graph_0, next)

    def _graph_1(self):
        word = self.next_word()

        if word.type == WordType.AND:
            next = self._graph_6
        elif word.type == WordType.noun:
            next = self._graph_2
        else:
            next = None

        self._transition(self._graph_1, next)

    def _graph_6(self):
        word = self.next_word()

        if word.type == WordType.prefix_condition:
            next = self._graph_1
        else:
            next = None

        self._transition(self._graph_6, next)

    def _graph_2(self):
        if not self.doing_cond and self.success_index > self.start_index:
            self.success_index = self.index

        if self.index == len(self.sentence):
            return self._end()

        word = self.next_word()

        if (
            word.type == WordType.infix_condition and
            not self.stage_2 and not self.no_conds and
            (not self.doing_cond or self.prev_state != self._graph_4)
        ):
            self.doing_cond = True
            next = self._graph_3
        elif (
            word.type == WordType.verb and
            (self.prev_tile_type != WordType.NOT or self.doing_cond or not self.stage_3)
        ):
            self.stage_2 = True
            self.no_conds = True
            self.doing_cond = False
            self.verb_got = True
            next = self._graph_3
        elif word.type == WordType.AND and self.prev_tile_type != WordType.NOT:
            next = self._graph_4
        else:
            next = None

        self._transition(self._graph_2, next)

    def _graph_3(self):
        word = self.next_word()
        self.stage_3 = True

        # Type 8?
        if word.type == WordType.noun or word.type == WordType.property:
            next = self._graph_5
        else:
            next = None

        self._transition(self._graph_3, next)

    def _graph_4(self):
        word = self.next_word()

        # Type 8?
        if word.type == WordType.noun or (word.type == WordType.property and self.stage_3):
            next = self._graph_2
        elif (
            word.type == WordType.verb and self.stage_3 and not self.doing_cond and
            self.prev_tile_type != WordType.NOT
        ):
            self.stage_2 = True
            self.no_conds = True
            self.verb_got = True
            next = self._graph_3
        elif (
            word.type == WordType.infix_condition and
            not self.no_conds and
            (self.prev_tile_type != WordType.AND or self.doing_cond)
        ):
            self.stage_2 = True
            self.doing_cond = True
            next = self._graph_3
        else:
            next = None

        self._transition(self._graph_4, next)

    def _graph_5(self):
        if self.verb_got and self.stage_3:
            self.success_index = self.index

        if self.index == len(self.sentence):
            return self._end()

        word = self.next_word()

        if (
            word.type == WordType.verb and self.doing_cond and self.prev_tile_type != WordType.NOT
        ):
            self.no_conds = True
            self.doing_cond = False
            self.verb_got = True
            next = self._graph_3
        elif word.type == WordType.AND and self.prev_tile_type != WordType.NOT:
            next = self._graph_4
        else:
            next = None

        self._transition(self._graph_5, next)

    def _end(self):
        if self.success_index > self.start_index:
            words = self.sentence[self.start_index:self.success_index]
            self.cleaned += SentenceCompiler().compile(words)
            if self.success_index >= len(self.sentence):
                return

        if self.try_again > self.start_index:
            self._reset(self.try_again)
            self.clean()
        elif self.index + 2 < len(self.sentence):
            self._reset(self.index + 1)
            self.clean()


class BabaMap:
    first_words_limit = 100
    default_rules = [
        Sentence([], AstNoun("TEXT", False), [], "IS", AstProperty("PUSH", False))
    ]

    def __init__(self):
        # Row is an array  of cells
        # A cell is an array of objects
        self.rows = []
        BabaData.get_data()
        self.custom_objects = {}

    def add_to_cell(self, object: BabaObject):
        self.rows[-1][-1].append(object)

    def new_row(self):
        self.rows.append([])

    def new_cell(self):
        self.rows[-1].append([])

    def add_sentence(self, sentence: str, all_is_color=True):
        new_cell = True
        self.new_row()

        def add_word(word, type, pops, new_cell):
            if word == "+":
                return False

            if new_cell:
                self.new_cell()

            if word != "-" and (type != BabaObjectType.Picture or word != "EMPTY"):
                data = self.custom_objects.get(word, None) or BabaData.data["words"].get(word, {})
                object = BabaObject(word, type, data)
                object.set_properties(props, all_is_color)
                self.add_to_cell(object)

            return True

        for word in sentence.split():
            type = BabaObjectType.Word
            props = []
            if ":" in word:
                word, props = word.rsplit(":", 1)
                props = props.upper().split(",")

            if not word:
                continue

            if word.endswith("!"):
                word = word.strip("!")
                type = BabaObjectType.Picture
            elif word.startswith("["):
                word = word.strip("[]")
                type = BabaObjectType.BackgroundWord
            elif not word[0].isalpha():
                alias = BabaData.aliases.get(word, None)
                if alias:
                    word = alias
                    type = BabaObjectType.Picture
                elif not props:
                    aliases = [
                        BabaData.aliases.get(emoji, None)
                        for emoji in grapheme.graphemes(word)
                    ]
                    if any(aliases):
                        for alias in aliases:
                            if not alias:
                                alias = "-"
                            new_cell = add_word(alias, BabaObjectType.Picture, [], new_cell)
                        continue

            word = word.upper()
            new_cell = add_word(word, type, props, new_cell)

    def has_picture(self, word, x, y):
        for object in self.objects(x, y):
            if object.type == BabaObjectType.Picture and object.word == word:
                return True

        return False

    def objects(self, x, y):
        if y < 0 or y >= len(self.rows):
            return []

        row = self.rows[y]
        if x < 0 or x >= len(row):
            return []

        return row[x]

    def has_word(self, x, y):
        return any(o.type != BabaObjectType.Picture for o in self.objects(x, y))

    def gather_rules(self, include_default=True):
        firstwords = []
        for y, row in enumerate(self.rows):
            for x, cell in enumerate(row):
                words = [obj for obj in cell if obj.is_word]
                if words:
                    vertical = not self.has_word(x, y-1) and self.has_word(x, y+1)
                    horizontal = not self.has_word(x-1, y) and self.has_word(x+1, y)
                    if vertical or horizontal:
                        for word in words:
                            firstwords.append(word.to_word(x, y, horizontal, vertical))

        if len(firstwords) > self.first_words_limit:
            raise TooComplex()

        rules = self._get_sentences(firstwords)

        if include_default:
            rules += self.default_rules

        return rules

    def _get_sentences(self, firstwords):
        sentences = []
        for word in firstwords:
            sentences += self._word_sentences(word)
        return sentences

    def _word_sentences(self, first_word: Word):
        sentences = []
        if first_word.horizontal:
            sentences += self._gather_sentences(first_word, True)
        if first_word.vertical:
            sentences += self._gather_sentences(first_word, False)

        cleaned_sentences = []
        for sentence in sentences:
            for clean in self._clean_sentence(sentence):
                cleaned_sentences.append(clean)
        return cleaned_sentences

    def _gather_sentences(self, first_word: Word, horizontal):
        sentence = [[first_word]]
        if horizontal:
            coords = [(x, first_word.y) for x in range(first_word.x + 1, len(self.rows[first_word.y]))]
        else:
            coords = [(first_word.x, y) for y in range(first_word.y + 1, len(self.rows))]

        for x, y in coords:
            sub_words = [obj for obj in self.objects(x, y) if obj.is_word]
            if not sub_words:
                break

            # TODO handle multiple word permutations
            if len(sub_words) > 1:
                raise TooComplex

            sentence.append([word.to_word(x, y, horizontal, not horizontal) for word in sub_words])

        return [[w[0] for w in sentence]]

    def _clean_sentence(self, sentence):
        parser = SyntaxParser(sentence)
        parser.clean()
        return parser.cleaned

    def to_data(self):
        return "\n".join(
            " ".join(
                " + ".join(
                    object.to_data()
                    for object in cell
                )
                if cell else "-"
                for cell in row
            )
            for row in self.rows
        )

    def add_custom_object(
        self,
        word: str,
        picture: lottie.objects.Animation,
        properties=[],
        picture_type: PictureType = PictureType.static,
        word_type: WordType = WordType.noun,
        color='#ffffff'
    ):
        self.custom_objects[word.upper()] = {
            "color": color,
            "word_type": word_type.name,
            "properties": properties,
            "picture": {
                "type": picture_type.name,
                "default": {
                    "file": picture
                },
            }
        }


class Collision:
    Nothing = 0
    Ground = 1
    Float = 2
    All = 3


class CompiledMap(BabaMap):
    You = {"YOU", "YOU2", "U2", "3D"}

    def __init__(self, baba_map: BabaMap):
        self.height = len(baba_map.rows)
        self.width = max(map(len, baba_map.rows), default=0)
        self.rows = self._matrix()
        self.all = set()
        self.instances = set()
        self.direction = None
        self.powered = False
        self.will_power = False
        self.instance_rows = self._matrix()
        self.rules = None
        self.won = False
        self.lost = False
        self.remove_later = set()

        for y, row in enumerate(baba_map.rows):
            for x, cell in enumerate(row):
                instance_cell = []
                for object in cell:
                    if object.word_type() == WordType.noun:
                        self.all.add(object.word)
                    if object.direction == Direction.static:
                        object.direction = Direction.right
                    instance = Instance(object, x, y, self)
                    self.instances.add(instance)
                    instance_cell.append(instance)
                self.instance_rows[y][x] = instance_cell
                self.rows[y][x] = cell

    def instances_at(self, x, y):
        if x < 0 or x >= self.width or y < 0 or y >= self.height:
            return []
        return self.instance_rows[y][x]

    def apply_rules(self, rules, transform, properties):
        self.rules = rules

        for instance in self.instances:
            instance.apply_rules(rules)

        if transform:
            for instance in self.instances:
                instance.become_is()
            self._cleanup()

        if properties:
            self.will_power = False
            for instance in self.instances:
                instance.apply_properties()
            self.powered = self.will_power

    def _cleanup(self):
        self.instances -= self.remove_later
        self.remove_later = set()

    def _matrix(self):
        matr = []
        for y in range(self.height):
            row = []
            for x in range(self.width):
                row.append([])
            matr.append(row)
        return matr

    def step(self, direction: Direction):
        # https://babaiswiki.fandom.com/wiki/Order_of_Operations
        self.direction = direction
        self._move()
        self.gather_rules()
        self.rules = self.gather_rules()
        self.apply_rules(self.rules, True, True)
        self._destroy()
        self._eat()
        self._make()
        self._win()
        self.rules = self.gather_rules()
        self.apply_rules(self.rules, False, True)

    def _move(self):
        you = set()
        move = set()
        nudges = {
            Direction.right: set(),
            Direction.up: set(),
            Direction.left: set(),
            Direction.down: set(),
        }
        # TODO fear = []
        # TODO shift = []
        for instance in self.instances:
            if instance.properties & self.You:
                you.add(instance)
            if instance.properties & {"MOVE", "AUTO", "CHILL"}:
                move.add(instance)
            for dir, list in nudges.items():
                if ("NUDGE" + dir.name.upper()) in instance.properties:
                    list.add(instance)

        if self.direction != Direction.static:
            for instance in you:
                instance.direction = self.direction
                instance.move(instance.direction, you)

        for instance in move:
            instance.auto_move(move)

        for dir, list in nudges.items():
            for instance in list:
                instance.move(dir, list)

    def has_property(self, property, x, y):
        for instance in self.instances_at(x, y):
            if property in instance.properties:
                return True
        return False

    def _destroy(self):
        to_destroy = []
        for row in self.instance_rows:
            for cell in row:
                sink = Collision.Nothing
                trigger_sink = Collision.Nothing
                defeat = Collision.Nothing
                boom = Collision.Nothing

                for object in cell:
                    if "BOOM" in object.properties:
                        boom = Collision.All

                    if "SINK" in object.properties:
                        sink |= object.collision
                    else:
                        trigger_sink |= object.collision

                    if "DEFEAT" in object.properties:
                        defeat |= object.collision

                sink &= trigger_sink

                if sink or boom or defeat:
                    for instance in cell:
                        if "SAFE" in instance.properties:
                            destroy = False
                        elif boom:
                            destroy = True
                        else:
                            collision = instance.collision
                            destroy = sink & collision
                            if not destroy and instance.properties & self.You:
                                destroy = defeat & collision

                        if destroy:
                            to_destroy.append(instance)

        for instance in to_destroy:
            instance.destroy()
        self._cleanup()

    def _eat(self):
        food = set()
        for instance in self.instances:
            food |= instance.eat()

        for instance in food:
            instance.destroy()

        self._cleanup()

    def _win(self):
        self.lost = True

        for row in self.instance_rows:
            for cell in row:
                win = Collision.Nothing
                you = Collision.Nothing
                for instance in cell:
                    if instance.properties & self.You:
                        you |= instance.collision
                    if "WIN" in instance.properties:
                        win |= instance.collision

                if you:
                    self.lost = False

                if win & you:
                    self.won = True
                    return

    def _make(self):
        for instance in self.instances:
            instance.make()

    def has_object(self, word, x, y):
        objects = self.objects(x, y)
        if word == "EMPTY":
            return len(objects) == 0
        elif word == "TEXT":
            for obj in objects:
                if obj.type != BabaObjectType.Picture:
                    return True
            return False
        else:
            for obj in objects:
                if obj.type == BabaObjectType.Picture and obj.word == word:
                    return True
            return False


class AppliedVerb:
    def __init__(self, verb):
        self.verb = verb
        self.yes = set()
        self.no = set()

    def add(self, word, negated):
        if negated:
            self.no.add(word)
        else:
            self.yes.add(word)

    def final(self):
        return self.yes - self.no


class AppliedVerbMap:
    def __init__(self):
        self._map = {}

    def __getitem__(self, key):
        verb = self._map.get(key, None)
        if verb is None:
            verb = AppliedVerb(key)
            self._map[key] = verb
        return verb

    def add_rule(self, rule: Sentence):
        verb = rule.verb
        object = rule.object
        if isinstance(object, AstProperty):
            if verb == "IS":
                verb = "IS_PROP"
            else:
                return
        elif not isinstance(object, AstNoun):
            return

        self[verb].add(object.word, object.negated)

    def get(self, key):
        return self._map.get(key, None)


class Instance:
    def __init__(self, object: BabaObject, x, y, map: CompiledMap):
        self.object = object
        self.x = x
        self.y = y
        self.map = map
        self.with_words = set()
        self.verbs = AppliedVerbMap()

    @property
    def collision(self):
        return Collision.Float if "FLOAT" in self.object.properties else Collision.Ground

    @property
    def is_object(self):
        return self.object.type == BabaObjectType.Picture

    @property
    def is_text(self):
        return not self.object

    @property
    def word(self):
        if self.is_object:
            return self.object.word
        return "TEXT"

    @property
    def properties(self):
        return self.object.properties

    @property
    def direction(self):
        return self.object.direction

    @direction.setter
    def direction(self, value: Direction):
        self.object.direction = value

    def apply_rules(self, rules):
        self.verbs = AppliedVerbMap()

        self.with_words = set(
            obj.word
            for obj in self.map.objects(self.x, self.y)
            if obj is not self.object
        )

        for rule in rules:
            if rule.applies_to(self):
                self.verbs.add_rule(rule)

    def apply_properties(self):
        properties_final = self.verbs["IS_PROP"].final()
        self.object.set_properties(properties_final)

        if "POWER" in properties_final:
            self.map.will_power = True

    def become_is(self):
        verb = self.verbs.get("IS")
        if not verb:
            return

        objects_final = verb.final()
        if self.word in objects_final or not objects_final:
            return

        has_all = self._make_objects(verb, True)

        if not has_all or self.word in verb.no:
            self.remove_from_map()

        for word in objects_final:
            self._make_object(word)

    def _make_object(self, word):
        data = BabaData.data["words"].get(word, {})
        if word == "TEXT":
            object = BabaObject(word, BabaObjectType.Word, data)
        else:
            object = BabaObject(word, BabaObjectType.Picture, data)

        instance = Instance(object, self.x, self.y, self.map)
        instance.object.direction = self.object.direction
        instance.apply_rules(self.map.rules)
        instance.apply_properties()
        instance.add_to_map()
        return instance

    def _all(self, verb):
        objects_all = self.map.all - verb.no
        objects_all.discard("TEXT")
        return objects_all

    def _make_objects(self, verb: AppliedVerb, exclude_self: bool):
        if not verb:
            return False

        objects_final = verb.final()
        has_all = "ALL" in objects_final

        if has_all:
            objects_final.discard("ALL")
            objects_final |= self._all(verb)

        if exclude_self:
            objects_final.discard(self.word)

        for word in objects_final:
            if word not in self.with_words:
                self._make_object(word)

        return has_all

    def make(self):
        self._make_objects(self.verbs.get("MAKE"), True)

    def auto_move(self, dont_push):
        if "CHILL" in self.object.properties:
            self.object.direction = random.choice([Direction.up, Direction.down, Direction.left, Direction.right])
            self.move(self.direction, dont_push)

        if "MOVE" in self.object.properties:
            if not self.move(self.direction, dont_push):
                self.direction = Direction((self.direction.value + 180) % 360)
                self.move(self.direction, dont_push)

        if "AUTO" in self.object.properties:
            self.move(self.direction)

    def move(self, direction, dont_push):
        if self._prepare_move(direction, dont_push):
            self._finalize_move()
            return True
        return False

    def _prepare_move(self, direction, dont_push):
        next_x, next_y = direction.step(self.x, self.y)
        if next_x < 0 or next_y < 0 or next_x >= self.map.width or next_y >= self.map.height:
            return False

        for obj in self.map.instances_at(next_x, next_y):
            if "STOP" in obj.properties:
                return False
            elif "PUSH" in obj.properties:
                if obj._prepare_move(direction, dont_push):
                    if obj not in dont_push:
                        obj.direction = direction
                        obj._finalize_move()
                else:
                    return False

        self.next_x = next_x
        self.next_y = next_y
        return True

    def _finalize_move(self):
        self.remove_from_map()
        self.x = self.next_x
        self.y = self.next_y
        self.add_to_map()

    def remove_from_map(self):
        self.map.instances_at(self.x, self.y).remove(self)
        self.map.objects(self.x, self.y).remove(self.object)
        self.map.remove_later.add(self)

    def add_to_map(self):
        self.map.objects(self.x, self.y).append(self.object)
        self.map.instances_at(self.x, self.y).append(self)
        self.map.remove_later.discard(self)

    def destroy(self):
        self._make_objects(self.verbs.get("HAS"), False)
        self.remove_from_map()

    def __repr__(self):
        return "<%s %s x=%s y=%s>" % (
            self.__class__.__name__,
            self.object.word,
            self.x,
            self.y
        )

    def eat(self):
        verb = self.verbs.get("EAT")
        if not verb:
            return set()

        to_eat = verb.final()
        if "ALL" in to_eat:
            to_eat = self._all(verb)
            to_eat.add("TEXT")

        return set(
            instance
            for instance in self.map.instances_at(self.x, self.y)
            if instance.word in to_eat
        )


class BabaData:
    assets_root: pathlib.Path = pathlib.Path(__file__).parent / "assets" / "baba"
    data = None
    custom_colors = {}
    aliases = {}
    default_colors = {}
    cache = {}

    @classmethod
    def get_data(cls):
        if cls.data is None:
            with open(cls.assets_root / "data.json", "r") as f:
                cls.data = json.load(f)

            for word, data in cls.data["words"].items():
                if "color" in data:
                    color = parse_color(data["color"])
                    cls.custom_colors[word] = color
                    if data.get("property_group", None) == "color":
                        cls.default_colors[word] = color

                if "aliases" in data:
                    for alias in data["aliases"]:
                        cls.aliases[alias] = word

        return cls.data

    @classmethod
    def import_file(cls, path: str) -> lottie.objects.Animation:
        if path in cls.cache:
            return cls.cache[path]

        full_path = cls.assets_root / path

        if not full_path.exists():
            return None

        with open(full_path) as f:
            animation = lottie.importers.core.import_tgs(f)

        if animation.out_point != 36:
            for index, layer in enumerate(animation.layers):
                layer.in_point = 12 * index
                layer.out_point = 12 * (index+1)

        cls.cache[path] = animation
        return animation


class BabaWriter:
    cell_size = 24
    padding = cell_size / 3

    def __init__(self):
        self.precomps = {}
        self.raw_precomps = {}
        self.characters = {}
        self.smol_characters = {}
        self.animation = lottie.objects.Animation()
        self.x = self.padding
        self.max_x = 0
        self.y = self.padding
        self.background = lottie.Color(0, 0, 0)
        BabaData.get_data()

    def set_map(self, map: BabaMap):
        for y, line in enumerate(map.rows):
            for x, cell in enumerate(line):
                for object in cell:
                    object.get_picture(map, x, y)
                    self.add_object(object, lottie.NVector(self.x, self.y))
                self.x += self.cell_size

            self.new_line()

    def new_line(self):
        self.y += self.cell_size

        if self.x > self.max_x:
            self.max_x = self.x

        self.x = self.padding

    def finalize(self):
        self.animation.out_point = 36

        for layer in self.animation.layers:
            layer.in_point = 0
            layer.out_point = self.animation.out_point

        self.max_x += self.padding
        self.y += self.padding

        self.animation.to_precomp()
        scale = 512 / max(self.cell_size * 11 / 3, self.max_x, self.y)
        height = int(math.ceil(self.y * scale))
        width = int(math.ceil(self.max_x * scale))
        self.animation.layers[0].transform.scale.value *= scale

        bg = lottie.objects.SolidColorLayer()
        bg.width = width
        bg.height = height
        bg.color = self.background
        self.animation.add_layer(bg)

    def debug(self):
        self.x = self.padding

        layout = {
            "n_block": 0,
            "n_char": 0,
            "precomp": lottie.objects.Precomp(),
            "x": 0,
            "y": 0,
        }

        def push_precomp():
            precomp = layout["precomp"]
            precomp.id = str(layout["n_block"])
            if layout["n_block"] % 2:
                self.recolor(precomp, "red", (1, 1, 1))
            self.animation.assets.append(precomp)
            self.add_layer(precomp.id, lottie.NVector(self.x, self.y))
            self.x += self.cell_size
            layout["x"] = 0
            layout["y"] = 0
            layout["precomp"] = lottie.objects.Precomp()
            layout["precomp"].layers = []
            layout["n_block"] += 1
            layout["n_char"] = 0
            if layout["n_block"] % 5 == 0:
                self.x = self.padding
                self.y += self.cell_size

        letter_size = 12

        for ch, data in sorted(BabaData.data["characters"].items()):
            smol_list = data.get("smol", [])
            if not smol_list:
                continue
            for smol in smol_list:
                ch_anim = BabaData.import_file(smol)
                for layer in ch_anim.layers:
                    clone = layer.clone()
                    clone.transform.position.value = lottie.NVector(layout["x"], layout["y"])
                    layout["precomp"].layers.append(clone)

                layout["n_char"] += 1

                layout["x"] += letter_size

                if layout["n_char"] == 2:
                    layout["x"] = 0
                    layout["y"] = letter_size

            push_precomp()

        self.y += self.cell_size
        self.max_x = self.cell_size * 5 + self.padding

    def add_object(self, object: BabaObject, position):
        if object.cache_key not in self.precomps:
            if object.raw_cache_key in self.raw_precomps:
                raw_precomp = self.raw_precomps[object.raw_cache_key]
            else:
                raw_precomp = self.make_object(object)
                self.raw_precomps[object.raw_cache_key] = raw_precomp

            precomp = raw_precomp.clone()

            precomp.width = raw_precomp.width
            precomp.height = raw_precomp.height
            if object.color:
                self.recolor(precomp, object.color, getattr(raw_precomp, "replace_color", None))
            object.apply_transform(precomp)
            precomp.id = object.cache_key
            precomp.name = object.word

            self.precomps[object.cache_key] = precomp
            self.animation.assets.append(precomp)
        else:
            precomp = self.precomps[object.cache_key]

        layer = self.add_layer(object.cache_key, position)

        if precomp.width != self.cell_size:
            layer.width = precomp.width
            layer.height = precomp.height
            layer.transform.position.value += lottie.NVector(
                (self.cell_size - precomp.width) / 2,
                (self.cell_size - precomp.height) / 2
            )

    def add_layer(self, precomp_id, position: lottie.NVector):
        layer = lottie.objects.PreCompLayer()
        layer.reference_id = precomp_id
        layer.width = self.cell_size + 2
        layer.height = self.cell_size + 2
        layer.transform.position.value = position.clone()
        self.animation.layers.insert(0, layer)
        return layer

    @staticmethod
    def best_color(precomp):
        colors = {}
        for layer in precomp.layers:
            for group in layer.shapes:
                layer_color = tuple(next(group.find_all(lottie.objects.Fill)).color.value.components[:3])
                layer_count = sum(r.size.value.x * r.size.value.y for r in group.find_all(lottie.objects.Rect))
                if layer_color not in colors:
                    colors[layer_color] = 0
                colors[layer_color] += layer_count

        best = 0
        best_color = tuple()
        for layer_color, count in colors.items():
            if count > best:
                best = count
                best_color = layer_color
        return best_color

    def recolor(self, precomp, color, replace_color):
        if isinstance(color, str):
            upper = color.upper()
            if upper in BabaData.custom_colors:
                lottie_color = BabaData.custom_colors[upper]
            else:
                try:
                    lottie_color = parse_color(color)
                except Exception:
                    return
        else:
            lottie_color = color

        if replace_color is not None:
            best_color = replace_color
        else:
            best_color = self.best_color(precomp)

        for fill in precomp.find_all((lottie.objects.Fill, lottie.objects.Stroke)):
            if tuple(fill.color.value.components[:3]) == best_color:
                fill.color.value = lottie_color

    def make_object(self, object: BabaObject):
        color = parse_color(object.default_color) if object.default_color else None

        if object.file:
            if isinstance(object.file, lottie.objects.Animation):
                word_anim = object.file.clone()
            else:
                word_anim = BabaData.import_file(object.file)

            if word_anim:
                if object.type == BabaObjectType.Picture:
                    self.anchor_center(word_anim)
                word_anim.to_precomp()
                word_anim.assets[0].width = word_anim.width
                word_anim.assets[0].height = word_anim.height
                return word_anim.assets[0]

        return self.make_word(object.word, object.type == BabaObjectType.BackgroundWord, color)

    def make_word(self, word, background, color):
        precomp = lottie.objects.Precomp()

        if len(word):
            n_chunks = int(math.floor(math.sqrt(len(word))))
            chunks = []
            chunk_length = len(word) // n_chunks
            i = -1
            for i in range(n_chunks-1):
                chunks.append(word[i * chunk_length:(i+1) * chunk_length])
            chunks.append(word[(i+1) * chunk_length:])
            max_chunk = chunk_length + len(word) % n_chunks

            padding = 0
            if background and len(word) > 1:
                padding = 1

            padding_x = padding+1 if padding else 0
            cell_size = self.cell_size - 2 * padding
            scale = 1 / max(len(chunks), max_chunk)
            letter_size = cell_size * scale
            y = padding + (cell_size - len(chunks) * letter_size) / 2

            for chunk in chunks:
                x = padding_x + (cell_size - len(chunk) * letter_size) / 2
                for char in chunk:
                    ch_anim, ch_size = self.character_file(char, letter_size)
                    if ch_anim:
                        for layer in ch_anim.layers:
                            clone = layer.clone()
                            clone.transform.position.value = lottie.NVector(x, y)
                            clone.transform.scale.value *= letter_size / ch_size
                            precomp.layers.append(clone)
                    x += letter_size
                y += letter_size

            if color and not background:
                self.recolor(precomp, color, None)

        if background:
            self.recolor(precomp, lottie.NVector(0, 0, 0), None)
            precomp.replace_color = (1, 1, 1)

            block = BabaData.import_file("block.json").clone()
            if color:
                self.recolor(block, color, precomp.replace_color)
                precomp.replace_color = tuple(color.components[:3])
            precomp.layers += block.layers

        precomp.width = precomp.height = self.cell_size
        return precomp

    def anchor_center(self, animation):
        center = lottie.NVector(animation.width, animation.height) / 2
        for layer in animation.layers:
            layer.transform.position.value = center.clone()
            layer.transform.anchor_point.value = center.clone()

    def character_file(self, ch, size):
        data = BabaData.data["characters"].get(ch, None)
        if not data:
            return None, 24

        if size > 12:
            return self.character_full_size(ch, data)

        if ch in self.smol_characters:
            ch_list = self.smol_characters[ch]
        else:
            ch_list = []
            for filename in data.get("smol", []):
                ch_list.append(BabaData.import_file(filename))
            self.smol_characters[ch] = ch_list

        if not ch_list:
            return self.character_full_size(ch, data)
        return random.choice(ch_list), 12

    def character_full_size(self, ch, data):
        if ch in self.characters:
            return self.characters[ch], 24
        anim = BabaData.import_file(data["text"]["file"])
        self.characters[ch] = anim
        return anim, 24


if __name__ == "__main__":
    parser = script.get_parser(formats=["html"])
    parser.add_argument(
        "text",
        nargs="*"
    )
    parser.add_argument("--debug", action="store_true")
    parser.add_argument("--custom", action="store_true")
    parser.add_argument("--background", default="#000000")
    parser.add_argument("--debug-tiled")
    parser.add_argument("--debug-directional")
    parser.add_argument("--rules", action="store_true")
    parser.add_argument("--run", action="store_true")
    ns = parser.parse_args()

    writer = BabaWriter()
    writer.background = parse_color(ns.background)

    if ns.debug:
        writer.debug()
    elif ns.custom:
        index = 0
        used = set()
        baba_map = BabaMap()
        baba_map.new_row()
        for word, data in BabaData.get_data()["words"].items():
            if "picture-url" not in data and "picture-file" in data and data["picture-file"] not in used:
                used.add(data["picture-file"])
                baba_map.new_cell()
                baba_map.add_to_cell(BabaObject(word, BabaObjectType.Picture))
                index += 1
                if index % 5 == 0:
                    baba_map.new_row()
        writer.set_map(baba_map)
    elif ns.debug_tiled:
        word = ns.debug_tiled.upper()
        data = BabaData.get_data()["words"][word]
        pic = [BabaObject(word, BabaObjectType.Picture, data)]
        txt = [BabaObject(word, BabaObjectType.Word, data)]
        nil = []
        baba_map = BabaMap()
        baba_map.rows = [
            [pic, nil, pic, pic, pic],
            [nil, txt, nil, nil, nil],
            [pic, nil, pic, pic, pic],
            [pic, nil, pic, pic, pic],
            [pic, nil, pic, pic, pic],
        ]
        writer.set_map(baba_map)
    elif ns.debug_directional:
        word = ns.debug_directional.upper()
        data = BabaData.get_data()["words"][word]
        baba_map = BabaMap()
        baba_map.rows = [
            [
                [BabaObject(word, BabaObjectType.Word, data)],
                [BabaObject(word, BabaObjectType.Picture, data, ["up"])],
                []
            ],
            [
                [BabaObject(word, BabaObjectType.Picture, data, ["left"])],
                [BabaObject(word, BabaObjectType.Picture, data, ["left"])],
                [BabaObject(word, BabaObjectType.Picture, data, ["right"])]
            ],
            [
                [],
                [BabaObject(word, BabaObjectType.Picture, data, ["down"])],
                []
            ]
        ]
        writer.set_map(baba_map)
    else:
        baba_map = BabaMap()

        for sentence in ns.text:
            for line in sentence.splitlines():
                baba_map.add_sentence(line)

        if ns.rules:
            for rule in baba_map.gather_rules():
                print(rule)
        if ns.run:
            CompiledMap(baba_map).step(Direction.static)
        writer.set_map(baba_map)
    writer.finalize()

    script.run(writer.animation, ns)
