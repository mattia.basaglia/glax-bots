import time
import pathlib

from mini_apps.telegram.events import NewMessageEvent
from mini_apps.apps.auth.user import User
from mini_apps.telegram.command import admin_command
from mini_apps.telegram.bots import (
    AdminMessageBot, WelcomeBot, AdminCommandsBot
)

from .animations import emoji_finder
# from .ai.conventions import ConventionAnswerer, Convention


class FloodCheck:
    def __init__(self, join):
        self.join = join
        self.last_media = self.join
        self.count = 0
        self.restricted = False

    def tick(self, has_media):
        now = time.time()
        delta = now - self.last_media
        if delta > 30 or now - self.join > 60:
            return None

        if has_media:
            if delta < 1:
                self.count += 5
            else:
                self.count += 1
            self.last_media = now

            return self.count > 10

        return False


class EurofursBot(WelcomeBot, AdminMessageBot, AdminCommandsBot):
    _asset_root = pathlib.Path(__file__).parent / "assets"

    def __init__(self, settings):
        super().__init__(settings)
        self.emoji_finder = emoji_finder
        # self.model = ConventionAnswerer(pathlib.Path(self.settings.get("model-path", self.get_server_path() / "eurofurs-model")))
    #
    # async def get_info(self, info: dict):
    #     await super().get_info(info)
    #     info["model_loaded"] = self.model._pipeline is not None
    #     info["conventions"] = len(self.model.conventions)
    #
    # def load_data(self):
    #     self.model.add_con(Convention(
    #         "Nordic Fuzzcon",
    #         "NFC",
    #         "Malmö",
    #         "Sweden",
    #         "February 21, 2024",
    #         "February 25, 2024",
    #         "1800",
    #     ))
    #     self.model.add_con(Convention(
    #         "Furvester",
    #         "FV",
    #         "Reutlingen",
    #         "Germany",
    #         "December 28, 2023",
    #         "January 1, 2024",
    #         "420",
    #     ))
    #     self.model.add_con(Convention(
    #         "Eurofurence",
    #         "EF",
    #         "Hamburg",
    #         "Germany",
    #         "September 3, 2023",
    #         "September 7, 2023",
    #         "3500",
    #     ))
    #     self.model.add_con(Convention(
    #         "Reffurence",
    #         "RFR",
    #         "Antwerp",
    #         "Belgium",
    #         "August 3, 2023",
    #         "August 6, 2023",
    #         "140",
    #     ))
    #
    # async def on_telegram_connected(self):
    #     self.model.ensure_pipeline()
    #     self.load_data()
    #
    # @admin_command()
    # async def answer(self, args: str, event: NewMessageEvent):
    #     answer = self.model.answer(args)
    #     await self.telegram.send_message(event.chat, answer.answer, reply_to=event.message)


class EurofursFloodBot(EurofursBot):
    def __init__(self, settings):
        super().__init__(settings)
        self.monitored = {}

    @admin_command()
    async def monitor(self, event: NewMessageEvent):
        """
        Monitors a user for flood
        """
        ids = await self.mentions_from_message(event)
        if len(ids) == 0:
            await event.reply("Who?")
            return

        now = time.time()
        for id in ids:
            self.monitored[int(id)] = FloodCheck(now)

        await event.reply("Monitoring %s for flood" % ", ".join(ids.values()))

    async def on_telegram_message(self, event: NewMessageEvent):
        if not event.sender:
            await event.get_sender()
        sender = event.sender

        if sender.id in self.monitored:
            check = self.monitored[sender.id]
            result = check.tick(event.message.media or len(event.text) > 250)
            if result is None:
                self.monitored.pop(sender.id)
            elif result and not check.restricted:
                name = event.client.user_name(sender)
                check.restricted = True
                try:
                    await self.toggle_mute(event, {sender.id: name}, True)
                finally:
                    await event.reply("%s is flooding\n %s" % (name, self.admin_message))
                return True

        return await super().on_telegram_message(event)

    async def on_user_join(self, telegram_user, chat, event):
        user = self.filter.filter_user(User.from_telegram_user(telegram_user))

        if not user:
            self.toggle_mute(event, {telegram_user.id: telegram_user.first_name}, True)
            return

        if not user.is_admin:
            self.monitored[user.id] = FloodCheck(time.time())

        super().on_user_join(telegram_user, chat, event)
