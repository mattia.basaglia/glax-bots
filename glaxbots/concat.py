"""
RFC9402 implementation
"""
import re
import random

import lottie
import lottie.objects
from lottie.utils.color import Color, ColorMode
from lottie.parsers.svg.importer import parse_color, color_table


from mini_apps.telegram.bot import TelegramBot
from mini_apps.telegram.utils import (
    static_sticker_file, animated_sticker_file, send_animated_sticker, send_sticker, InlineHandler, MessageFormatter
)
from mini_apps.telegram.events import NewMessageEvent, InlineQueryEvent
from .animations import load_svg


class AstNode:
    def render(self, renderer):
        return renderer.unsupported(self)

    def dump(self, indent=0):
        text = self.__class__.__name__ + "(\n"
        for n, v in vars(self).items():
            text += self._dump_indent(indent + 1)
            text += "%s = %s\n" % (n, self._dump_value(v, indent + 1))

        text += self._dump_indent(indent)
        text += ")"
        return text

    def _dump_value(self, value, indent):
        if isinstance(value, AstNode):
            return value.dump(indent)
        elif isinstance(value, (tuple, list)):
            text = "[\n"
            for v in value:
                text += self._dump_indent(indent + 1)
                text += self._dump_value(v, indent + 1) + ",\n"
            text += self._dump_indent(indent)
            text += "]"
            return text
        else:
            return value

    def _dump_indent(self, indent):
        return " " * (indent * 2)


class Subject(AstNode):
    def __init__(self, id):
        self.id = id


class Poes(Subject):
    def __init__(self, id):
        self.id = id

    def __str__(self):
        return "poes%s" % self.id

    def render(self, renderer):
        return renderer.poes(self)


class Yarn(Subject):
    def __str__(self):
        return "@%s" % self.id

    def render(self, renderer):
        return renderer.yarn(self)


class PartialPoes(Subject):
    def __init__(self, text, id=0):
        super().__init__(id)
        self.text = text

    def __str__(self):
        return self.text

    def render(self, renderer):
        return renderer.partial_poes(self)


class NotPoes(Subject):
    def __init__(self, text, id):
        super().__init__(id)
        self.text = text

    def __str__(self):
        return "%s%s" % (self.text, self.id)

    def render(self, renderer):
        return renderer.not_poes(self)


class NaryNode(AstNode):
    def __init__(self, children=None):
        self.children = children or []


class Over(NaryNode):
    def __str__(self):
        return "<%s>" % " / ".join(map(str, self.children))

    def render(self, renderer):
        return renderer.over(self.children)


class NextTo(NaryNode):
    def __str__(self):
        return " + ".join(map(str, self.children))

    def render(self, renderer):
        return renderer.next_to(list(map(renderer.render_node, self.children)))


class UnaryNode(AstNode):
    def __init__(self, child):
        self.child = child


class Box(UnaryNode):
    def __str__(self):
        return "[%s]" % self.child

    def render(self, renderer):
        return renderer.box(self.child, type(self))


class RoundBox(Box):
    def __str__(self):
        return "(%s)" % self.child


class SoftBox(Box):
    def __str__(self):
        return "{%s}" % self.child


class Table(Box):
    def __str__(self):
        return "_%s_" % self.child


class Sequence(NaryNode):
    def __str__(self):
        return " => ".join(map(str, self.children))


class Parser:
    """
    See RFC9402 https://datatracker.ietf.org/doc/html/rfc9402
SEQUENCE = POSITION / POSITION "=>" SEQUENCE
POSITION = ADJACENT
ADJACENT = OVER / ADJACENT "+" OVER
OVER = MULTIPLE / MULTIPLE "/" POSITION
MULTIPLE = POESCONT / NUMBER [ "*" ] MULTIPLE / NUMBER "/" MULTIPLE
POESCONT = SUBJECT [ NUMBER ] / OPT-PARTIAL-POES CONTAINER OPT-PARTIAL-POES
CONTAINER = "[" OPT-POSITION "]" / "(" OPT-POSITION ")" / "{" OPT-POSITION "}" / "<" POSITION ">" / "_" POSITION "_"
OPT-POSITION = [ POSITION ]
SUBJECT = POES / 1*ALPHA / "@"
POES = "poes" / PARTIAL-POES
OPT-PARTIAL-POES = [ PARTIAL-POES ]
PARTIAL-POES = "p" / "o" / "e" / "s" / "po" / "oe" / "es"
NUMBER = 1*DIGIT
DIGIT = "0" / "1" / "2" / "3" / "4" / "5" / "6" / "7" / "8" / "9"
ALPHA =  %x41-5A / %x61-7A
    """
    token_re = re.compile(
        r"(?P<number>[0-9]+)|(?P<keyword>[a-zA-Z]+)|(?P<operator>[\[\](){}*+/<>@_])|(?P<arrow>=>)"
    )

    partial = {"p", "o", "e", "s", "po", "oe", "es", "poe", "oes"}
    poes = {"poes", "poezen", "cat", "cats", "kat", "katten", "kitty", "kitten", "kittens", "poesje", "poesjes"}

    class ParseError(Exception):
        def __init__(self, pos, string, message):
            super().__init__(message)
            self.pos = pos
            self.string = string

        def context(self):
            return self.string + "\n" + " " * self.pos + "^\n"

    def _get(self):
        self._go_to_token(self.index + 1)

    def _go_to_token(self, index):
        self.index = index
        self.lookahead = self.tokens[self.index] if self.index < len(self.tokens) else None

    def _error(self, msg):
        raise self.ParseError(self.lookahead.start() if self.lookahead else len(self.input), self.input, msg)

    def parse(self, poes: str):
        self.input = poes
        self.tokens = list(self.token_re.finditer(poes))
        if not self.tokens:
            self._error("Expected Poescont")
        self._go_to_token(0)
        return self._sequence()

    def _sequence(self):
        seq = Sequence()
        while True:
            self._ids = {}
            seq.children.append(self._position())
            if self._token("arrow"):
                self._get()
            elif self.lookahead is None:
                break
            else:
                self._error("Expected =>")
        return seq

    def _position(self):
        return self._adjacent()

    def _over(self):
        seq = Over()
        while True:
            seq.children.append(self._multiple())
            if self._token("operator") == "/":
                self._get()
            else:
                break
        return seq.children[0] if len(seq.children) == 1 else seq

    def _adjacent(self):
        seq = NextTo()
        while True:
            seq.children.append(self._over())
            if self._token("operator") == "+":
                self._get()
            else:
                break
        return seq.children[0] if len(seq.children) == 1 else seq

    def _in(self, group, check):
        return self.lookahead and self.lookahead.group(group) and self.lookahead.group(group) in check

    def _token(self, group):
        return self.lookahead.group(group) if self.lookahead else None

    def _multiple(self):
        if self._token("number"):
            count = int(self._token("number"))
            self._get()
            if count <= 0:
                self._error("Must have a positive number of poezen")

            objects = NextTo()

            if self._token("operator") == "*":
                self._get()
            elif self._token("operator") == "/":
                self._get()
                objects = Over()

            index = self.index
            for i in range(count):
                self._go_to_token(index)
                objects.children.append(self._multiple())
            return objects
        return self._posecont()

    def _posecont(self):
        if self._in("keyword", self.partial):
            partial = PartialPoes(self._token("keyword"))
            self._get()
            if self._in("operator", "([{<_"):
                next_to = NextTo()
                next_to.children.append(partial)
                self._container(next_to)
                return next_to
            return partial
        elif self._in("operator", "([{<_"):
            return self._container(None)
        else:
            return self._subject()

    def _container(self, next_to):
        if self._token("operator") == "[":
            close = "]"
            container = Box
        elif self._token("operator") == "(":
            close = ")"
            container = RoundBox
        elif self._token("operator") == "{":
            close = "}"
            container = SoftBox
        elif self._token("operator") == "_":
            close = "_"
            container = Table
        elif self._token("operator") == "<":
            close = ">"
            container = None
        else:
            self._error("Expected container")

        self._get()
        if container and self._token("operator") == close:
            self._get()
            item = container(None)
        else:
            item = self._position()
            if container:
                item = container(item)
            if self._token("operator") != close:
                self._error("Expected %s" % close)
            self._get()

        if next_to:
            next_to.children.append(item)

        if self._in("keyword", self.partial):
            partial = PartialPoes(self._token("keyword"))
            self._get()
            if next_to:
                next_to.children.append(partial)
            else:
                return NextTo([item, partial])

        return item

    def _get_id(self, name):
        if self._token("number"):
            id = int(self._token("number"))
            self._get()
        else:
            id = self._ids.get(name, 0)
            self._ids[name] = id + 1
        return id

    def _subject(self):
        if self._token("operator") == "@":
            self._get()
            return Yarn(self._get_id("yarn"))

        if not self._token("keyword"):
            self._error("Expected poes")

        kw = self._token("keyword")
        self._get()

        if kw in self.poes:
            return Poes(self._get_id("poes"))

        if kw in self.partial:
            return PartialPoes(kw)

        while self._token("keyword"):
            kw += " " + self._token("keyword")
            self._get()
        return NotPoes(kw, self._get_id(kw))


class RenderedNode:
    @classmethod
    def empty(cls):
        return cls(lottie.objects.Group(), lottie.objects.BoundingBox())

    def __init__(self, shape: lottie.objects.Group, bbox: lottie.objects.BoundingBox):
        self.shape = shape
        self.bbox = bbox

    def _lerp(self, bbox: lottie.objects.BoundingBox, align_x=0.5, align_y=0.5):
        return lottie.NVector(
            bbox.x1 * (1 - align_x) + bbox.x2 * align_x,
            bbox.y1 * (1 - align_y) + bbox.y2 * align_y

        )

    def fit(self, bbox: lottie.objects.BoundingBox, align_x=0.5, align_y=0.5):
        if self.bbox.isnull() or bbox.isnull():
            return self.shape

        scale = 1

        if self.bbox.width > bbox.width:
            scale = bbox.width / self.bbox.width

        if self.bbox.height > bbox.height:
            scale = min(scale, bbox.height / self.bbox.height)

        self.shape.transform.scale.value *= scale
        bp = self._lerp(bbox, align_x, align_y)
        sp = self._lerp(self.bbox, align_x, align_y)
        pos = lottie.NVector(bp.x - sp.x * scale, bp.y - sp.y * scale)
        self.shape.transform.position.value += pos

        return self.shape


class Renderer:
    colors = {
        "witte": "white",
        "grijze": "grey",
        "zwarte": "black",
        "roze": "pink",
        "paarse": "purple",
        "blauwe": "blue",
        "groene": "green",
        "gele": "yellow",
        "bruine": "brown",
        "oranje": "orange",
        "rode": "red",
        "wit": "white",
        "grijs": "grey",
        "zwart": "black",
        "roze": "pink",
        "paars": "purple",
        "blauw": "blue",
        "groen": "green",
        "geel": "yellow",
        "bruin": "brown",
        "oranje": "orange",
        "rood": "red"
    }
    derg = {"derg", "durg", "dragon", "glax", "draak", "draaken", "draakje", "draakjes"}

    def __init__(self, max_poes=32):
        self.warnings = set()
        self.generated = {}
        self.max_poes = max_poes

    def render(self, node: Sequence):
        self.rendered_poezen = 0
        anim = lottie.objects.Animation()
        anim.out_point = 180
        if node.children:
            node_duration = anim.out_point / len(node.children)
            box = lottie.objects.BoundingBox(0, 0, anim.width, anim.height)
            for i, item in enumerate(node.children):
                layer = anim.add_layer(lottie.objects.ShapeLayer())
                layer.in_point = i * node_duration
                layer.out_point = (i+1) * node_duration
                layer.add_shape(self.render_node(item).fit(box))
        return anim

    def render_node(self, node: AstNode) -> RenderedNode:
        try:
            return node.render(self)
        except Exception as e:
            raise
            self.warnings.add(str(e))

    def load_svg(self, file):
        anim = load_svg("../poes/" + file)
        group = lottie.objects.Group()
        group.shapes = anim.layers[0].shapes + [group.transform]
        anim.layers[0].transform.clone_into(group.transform)
        return RenderedNode(group, lottie.objects.BoundingBox(0, 0, anim.width, anim.height))

    def _limit(self):
        if self.rendered_poezen >= self.max_poes:
            self.warnings.add("Too many poezen!")
            return True
        self.rendered_poezen += 1
        return False

    def _colorize(self, file, color_map):
        svg = self.load_svg(file)

        color_map = {
            tuple(parse_color(k)): v
            for k, v in color_map.items()
        }

        for styler in svg.shape.find_all((lottie.objects.Fill, lottie.objects.Stroke)):
            choices = color_map.get(tuple(styler.color.value), None)
            if choices:
                styler.color.value = random.choice(choices)
        return svg

    def _color_orange(self):
        return Color(random.uniform(0.07, 0.12), random.uniform(0.7, 1), random.uniform(0.7, 1), mode=ColorMode.HSV)

    def _color_gray(self):
        return Color(0, 0, random.uniform(0.3, 0.8), mode=ColorMode.HSV)

    def _color_cream(self):
        return Color(0.14, random.uniform(0, 0.04), 1, mode=ColorMode.HSV)

    def _color_black(self):
        return Color(0.14, random.uniform(0, 0.07), random.uniform(0, 0.1), mode=ColorMode.HSV)

    def lighter(self, color):
        light = color.clone()
        light.v = 1 - (1 - light.v) * random.uniform(0.5, 0.8)
        light.s *= random.uniform(0.6, 0.8)
        return light

    def darker(self, color):
        dark = color.clone()
        dark.v *= random.uniform(0.5, 0.8)
        return dark

    def _generate_poes(self, base_color):
        mode = random.randint(0, 1) if not base_color else 1
        extra = {}

        if mode == 0: # bicolor / calico
            filename = "poes-2col.svg"

            colors = [
                self._color_cream(),
                self._color_orange(),
                self._color_black(),
            ]
            submode = random.randint(0, 7)
            if submode == 0: # monochrome
                colors = [random.choice(colors)]
            elif submode > 2: # 2 colors (not calico)
                colors.pop(random.randint(0, 2))

            lightest = colors[0]
            for color in colors:
                if color.v > lightest.v:
                    lightest = color
            main = random.choice(colors)
            extra = {"#4290bd": [lightest, main]}
            light_colors = dark_colors = colors

        else: # Tabby
            filename = "poes-tabby.svg"

            submode = random.randint(0, 1)
            if base_color:
                main = base_color
            elif submode == 1: # Ginger / brown
                main = Color(random.uniform(0.07, 0.12), random.uniform(0.7, 1), random.uniform(0.7, 1), mode=ColorMode.HSV)
            else: # Gray / black
                main = Color(0, 0, random.uniform(0.3, 0.8), mode=ColorMode.HSV)

            v_dark = main.clone()
            v_dark.v *= random.uniform(0.3, 0.4)
            v_dark.s *= random.uniform(0.3, 0.6)

            light = main.clone()
            light.v = 1 - (1 - light.v) * random.uniform(0.5, 0.8)
            light.s *= random.uniform(0.6, 0.8)

            light_colors = [self.lighter(main), main, Color(1, 1, 1)]
            dark_colors = [self.darker(main), main, v_dark]

        outline = main.clone()
        outline.v *= 0.5
        outline.s *= 0.8

        eye = Color(random.uniform(0.12, 0.7), 1, 1, mode=ColorMode.HSV)
        if eye.h > 0.6:
            eye.s = 0.6

        in_colors = {
            **extra,
            "#3250b0": [main],
            "#c4d9f5": light_colors,
            "#292f75": dark_colors,
            "#1d2848": [outline],
            "#f01d0a": [eye]
        }
        if main.v < 0.2:
            in_colors["#aadffc"] = [Color(1, 1, 1)]

        return filename, main, in_colors

    def generate(self, what, id_prefix, id, base_color=None):
        self.generated.setdefault(id_prefix, {})
        if id in self.generated[id_prefix]:
            old = self.generated[id_prefix][id]
            return RenderedNode(old.shape.clone(), old.bbox)

        if self._limit():
            return RenderedNode.empty()

        if what == "poes":
            filename, main, in_colors = self._generate_poes(base_color)
        elif what in {"yarn", "towel", "glax"}:
            filename = what + ".svg"
            if base_color:
                main = base_color
            else:
                main = Color(random.uniform(0, 1), random.uniform(0.5, 0.8), random.uniform(0.7, 1), mode=ColorMode.HSV)

            outline = main.clone()
            outline.v *= 0.5
            outline.s *= 0.8

            in_colors = {
                "#3250b0": [main],
                "#1d2848": [outline],
            }

            if what == "glax":
                in_colors["#c4d9f5"] = [self.lighter(main)]
                in_colors["#292f75"] = [self.darker(main)]

        else:
            return self.unsupported(id_prefix)

        item = self._colorize(filename, in_colors)
        self.generated[id_prefix][id] = item
        return RenderedNode(item.shape.clone(), item.bbox)

    def poes(self, node: Poes):
        return self.generate("poes", "poes", node.id)

    def yarn(self, node: Yarn):
        return self.generate("yarn", "yarn", node.id)

    def partial_poes(self, node: PartialPoes):
        return self.unsupported("partial poes")

    def unsupported(self, node):
        self.warnings.add("Redering not supported for %s" % node)
        return RenderedNode.empty()

    def _color_adj(self, word):
        if word in self.colors:
            return self.colors[word]
        if word in color_table:
            return word
        return None

    def not_poes(self, node: NotPoes):
        if node.text in self.derg:
            return self.load_svg("glax.svg")
        elif node.text == "towel":
            return self.generate("towel", "towel", node.id)
        elif node.text == "glass":
            return self.load_svg("glass.svg")
        elif " " in node.text:
            words = node.text.split(" ")
            if len(words) == 2:
                eng_color = self._color_adj(words[0])
                if eng_color:
                    what = words[1]
                    if what in Parser.poes:
                        if eng_color == "blue":
                            return self.load_svg("poes-tabby.svg")
                        what = "poes"
                    elif what in self.derg:
                        what = "glax"

                    color = Color(*color_table[eng_color]).converted(ColorMode.HSV)
                    return self.generate(what, node.text, node.id, color)
        return self.unsupported(node.text)

    def over(self, nodes):
        rendered = RenderedNode.empty()
        children = []

        y = 0
        for node in nodes:
            rnode = self.render_node(node)
            children.append(rnode)
            if not rnode.bbox.isnull():
                new_box = lottie.objects.BoundingBox(rnode.bbox.x1, rnode.bbox.y1 + y, rnode.bbox.x2, rnode.bbox.y2 + y)
                rnode.new_box = new_box
                rendered.bbox.expand(new_box)
                y = new_box.y2 - 50
            else:
                rnode.new_box = None

        for child in children:
            if child.new_box:
                child.new_box.x2 = rendered.bbox.x2
                child.new_box.x1 = rendered.bbox.x1
                rendered.shape.add_shape(child.fit(child.new_box))

        return rendered

    def next_to(self, rendered_nodes):
        rendered = RenderedNode.empty()
        children = []

        x = 0
        for rnode in rendered_nodes:
            children.append(rnode)
            if not rnode.bbox.isnull():
                new_box = lottie.objects.BoundingBox(rnode.bbox.x1 + x, rnode.bbox.y1, rnode.bbox.x2 + x, rnode.bbox.y2)
                rnode.new_box = new_box
                rendered.bbox.expand(new_box)
                x = new_box.x2
            else:
                rnode.new_box = None

        for child in children:
            if child.new_box:
                child.new_box.y1 = rendered.bbox.y2 - child.bbox.height
                child.new_box.y2 = rendered.bbox.y2
                rendered.shape.add_shape(child.fit(child.new_box))

        return rendered

    def box(self, node: AstNode, type):
        if type is Box:
            if isinstance(node, NotPoes) and node.text == "closed":
                return self.load_svg("box-closed.svg")

            fg = self.load_svg("box-fg.svg")
            bg = self.load_svg("box-bg.svg")
            bbp = lottie.NVector(218, 94)
            inner_box = lottie.objects.BoundingBox(bbp.x, bbp.y, bbp.x + 768, bbp.y + 512)
            bg.shape.transform.position.value += lottie.NVector(228, 104)
        elif type is RoundBox:
            fg = self.load_svg("bed-fg.svg")
            bg = self.load_svg("bed-bg.svg")
            bbp = lottie.NVector(115, 159)
            inner_box = lottie.objects.BoundingBox(bbp.x, bbp.y, bbp.x + 768, bbp.y + 512)
        elif type is Table:
            fg = self.load_svg("table.svg")
            bbp = lottie.NVector(218, 226)
            inner_box = lottie.objects.BoundingBox(bbp.x, bbp.y, bbp.x + 768, bbp.y + 512)
            bg = None

        rendered = RenderedNode.empty()
        rendered.bbox = fg.bbox
        rendered.shape.add_shape(fg.shape)
        if node:
            rendered.shape.add_shape(self.render_node(node).fit(inner_box, 0.5, 1))
        if bg:
            rendered.shape.add_shape(bg.shape)
        return rendered


def poescont_render(string):
    try:
        ast = Parser().parse(string)
        renderer = Renderer()
        animation = renderer.render(ast)
        return animation, renderer.warnings
    except Exception as e:
        return lottie.objects.Animation(), [e]


class ConCatBot(TelegramBot):
    @TelegramBot.bot_command
    async def concat(self, args, event: NewMessageEvent):
        """
        Renders a sticker based on the RFC 9402 specification
        """
        if not args:
            await event.client.send_message(event.chat, "You have to send me some text with that command")
            return

        animation, warnings = poescont_render(args)

        if len(animation.layers) == 1:
            await send_sticker(event.client, event.chat, static_sticker_file(animation))
        else:
            await send_animated_sticker(event.client, event.chat, animated_sticker_file((animation)))

        if warnings:
            msg = MessageFormatter()
            for warning in warnings:
                msg += "%s\n" % warning
                if isinstance(warning, Parser.ParseError):
                    msg.pre(warning.context())
                    msg += "\n"

            await event.client.send_message(event.chat, msg.text, formatting_entities=msg.entities)

    async def on_telegram_inline(self, event: InlineQueryEvent):
        async with InlineHandler(event) as inline:
            if inline.query:
                animation, warnings = poescont_render(inline.query)

                if len(animation.layers) == 1:
                    inline.sticker(static_sticker_file(animation))
                else:
                    inline.animated_sticker(animated_sticker_file((animation)))
