import sys

from mini_apps.settings import Settings

settings = Settings.load_global()

app = settings.apps["eurofurs"]
app.load_data()

print(app.model.answer(sys.argv[1]))
