from mini_apps.settings import Settings

settings = Settings.load_global()

app = settings.apps["eurofurs"]
app.model.ensure_pipeline()
